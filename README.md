This repository is designed to be a working cashe of all my python
practice projects. 

---
## Directories 

1. **./gun_violence/:** This directory holds a data set and a jupyter
   notebook of some analysis on mass shootings that have taken place
   in the USA since 1982- see README_gv.md for more details
   
2. **./leeHist/:** This directory holds the data and the jupyter
   notebook to reduce, import and analyze low energy electron
   distributions from the LEE fortran code- see README_lee.md for more
   details
   
3. **./skill_share/:** This directory holds directories that are
   practice of my python skills from some of the online classes from
   the skill share website. There are a couple directories that are
   not my-own but examples of good code, or downloaded homework for
   the classes.
	   -My titanic ML database can be found in:
   `./skill_share/machine_learning/titanic_prediction/`

---

