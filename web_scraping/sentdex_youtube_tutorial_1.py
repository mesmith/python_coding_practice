import bs4 as bs
import urllib.request as ulr


sauce = ulr.urlopen("https://pythonprogramming.net/parsememcparseface/").read()
soup = bs.BeautifulSoup(sauce,'lxml') # being explicit about the parser with 'lxml'

#lets just see what the soup is
print(soup)

#look at the title
print(soup.title)

#get the tag
print(soup.title.name)

#get the characters that make the string of the title (what humans read)
print(soup.title.string,type(soup.title.string) )
print(soup.title.text,type(soup.title.text))

#get elements of paragraphs
print(soup.p)
print(soup.find_all('p'))

print("*****************************************")
# lets systematically get the paragraphs
for paragraph in soup.find_all('p'):
    print(paragraph.text)


print("*****************************************")
#problem is sometimes text on the page is not in paragraph tags
print(soup.get_text())


print("*****************************************")
#find all the links
for links in soup.find_all('a'): 
    print(links.text) #text of the tag
    print(links.get('href')) #actual links 
