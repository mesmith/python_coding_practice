import bs4 as bs
import urllib.request as ulr


sauce = ulr.urlopen("https://pythonprogramming.net/parsememcparseface/").read()
soup = bs.BeautifulSoup(sauce,'lxml') # being explicit about the parser with 'lxml'

##nav bar
nav = soup.nav
print(nav)

for link in nav.find_all('a'):
    print(link.get('href'))

body = soup.body 
for paragraph in body.find_all('p'):
    print(paragraph.text)

##find all the text between div tags! 
for div in soup.find_all("div", class_='body'):
    print(div.text)
