import requests
from bs4 import BeautifulSoup

## put the request in
request = requests.get('https://www.google.com')

## check the status
## 200 = ok
## https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
#print(request.status_code)

## look at the header
#print(request.headers)

## return the source of the page
src = request.content

## make a soup object from the source to extract info
soup = BeautifulSoup(src,'lxml')

#look at the links
links = soup.find_all('a')
#print(links)
#print('\n')

## check out particular lnks by their text value
#for link in links:
#    if "YouTube" in link.text:
#        print(link,link.attrs['href'], sep='\n')

## go to govenrment website and get the breifings from the main page list 
src = requests.get('https://www.whitehouse.gov/briefings-statements/').content
soup = BeautifulSoup(src,'lxml')
links = soup.find_all('a')

i=0
for link in links:
#    print(link,'\n')
    i+=1
print('total links', i)
i=0
for link in soup.find_all('a', class_='nav__link'):
#    print(link,'\n')
    i+=1
print('breifings and statement main list', i)
