Repository for practicing web scraping with Beautiful Soup

---
## Directories 


---
## What you need to run 

- This is run through the terminal
    - open a terminal and navigate to this folder 
    -  to run `python sentdex_youtube_tutorial.py`

- **beautifulsoup4** 
	- installed using conda: conda install -c anaconda beautifulsoup4

- **lxml package** 
	- installed using conda: conda install -c anaconda lxml
	- Pythonic binding for the C libraries libxml2 and libxslt.

- **pandas**
    - installed with pip3 (pip install pandas)


---

## Git Repository

This is a repo that is located on BitBucket, which runs git. 
Important instructions: 

- git status: shows the status of the files changed in the local dir. 
- git pull: pulls from the repo the lastest chages if working with a 
    group/ multiple computers- this is important to avoid merge errors
- git add <filename> or git add . 
    - <filename> adds that specific file to be committed 
    - . adds all the changed files 
- git commit -m 'message for commit'
    - without the -m then git will use the default editor to force a 
    commit message, which is usually vi/vim (eww)
- git push 
    - pushed all the changes from the local computer to the repo 
