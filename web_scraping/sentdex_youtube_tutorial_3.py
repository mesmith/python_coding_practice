import bs4 as bs
import urllib.request as ulr
import pandas as pd

sauce = ulr.urlopen("https://pythonprogramming.net/parsememcparseface/").read()
soup = bs.BeautifulSoup(sauce,'lxml') # being explicit about the parser with 'lxml'

# get the information from the table on the website
table = soup.table # or = soup.find('table')
print(table)

table_rows = table.find_all('tr') #all rows including header
table_headers = table.find_all('th') #just header
table_data = table.find_all('td')

print(table_headers)
print(table_rows)
print(table_data)

print("**********************************************")
for tr in table_rows:
    td = tr.find_all('td')
    row = [i.text for i in td]
    print(row)

print('Pandas stuff:')
# will go to the website and get all the tables it can fins and return a list of dataframes- because there might be multiple tables
dfs = pd.read_html("https://pythonprogramming.net/parsememcparseface/")

# look at the data head() so we can see how many tables picked up
for df in dfs:
  #  print(df.head())
    print(df)

print('reading site maps')
sauce = ulr.urlopen("https://pythonprogramming.net/sitemap.xml").read()
soup = bs.BeautifulSoup(sauce,'xml') # being explicit about the parser with 'lxml'
#print(soup)

#to find all the urls' they have the loc tag
for tag in soup.find_all('loc'):
    print(tag.text)
