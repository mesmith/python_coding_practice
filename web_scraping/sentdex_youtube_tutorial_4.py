# need to have pyqt4 installed to work through this tutorial... not a priority at the moment

import bs4 as bs
import urllib.request as ulr


sauce = ulr.urlopen("https://pythonprogramming.net/parsememcparseface/").read()
soup = bs.BeautifulSoup(sauce,'lxml') # being explicit about the parser with 'lxml'
# we know it's js test by looking at the html code from the webpage and finding the 
js_test = soup.find('p',class_='jstest')
print(js_test)

# this prints 'y u bad tho?' instead of the expected 'look at you shinnin!'
# this is because your not a web browser! your a script ripping data off the website and
# therefore you wont always get the actual strings/values you see on the webpage
# to fix this need to mimic being a client/browser to get the values we are looking for
