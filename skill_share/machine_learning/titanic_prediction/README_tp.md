A jupyter notebook collection aimed at using machine learning to predict the survival of passengers on the titanic voyage 

---
## Files & Sub-directories

1. **./data/:** The directroy that hold the titanic datasets
    - **./data/titanic_test.csv/:** csv file that holds the test file that the machine learning algorithm I impliment will use to try and predict survival
    - **./data/titanic_train.csv/:** csv file that holds the training data for the machine learning algorithm

2. **./jack-dies/:** An excellent example of efficient and clean coding & one that has multiple examples of machine learning algorithms from scikit-learn. Not my own work but that of Arkham and it can be found at: https://github.com/Arkham/jack-dies

3. **./output_working/:** Directory of outputs from the working file
    - **./output_working/titanic_solution.csv:** A file from the ML decision tree of the passenger id's and their survival outcome based on the titanic_train.csv file. The values represent the likely death of the passenger (0) or their likely survival (1).
    - **./output_working/decision_tree.csv:** Output from the first decision tree
    - **./output_working/decision_tree_two.csv:** output from the second decision tree with better fitting to data
    - **./output_working/decision_tree_two.dot:** the file you can make a graphic of the decision tree with
    - **./output_working/random_forest.csv:** output from the random forest
    - **./output_working/gender_model.csv:** the first model for the gender
      (m/f) data output
    - **./output_working/gbm.csv:** gradient boosting classifier fit
      to the data
   
4. **./titanic_main.ipynb:** The main release version of the analysis of the titanic historical data -> currently empty as I am still working though the information I want to show 

5. **./titanic_working.ipynb:** My working file that has some graphs of the interesting correlation of survival with the available demographics. They are survival vs.: age, gender, class and port of embarkment. Currently working on the survival vs. age demographic. In the second half of the notebook I run through a skill share example of running a basic decision tree maching learning algorithm on the training data set and output its score and the file titanic_solution.csv. 

6. **./titanic_rough.ipynb:** My own coding of the rough copy of the main file, has a bit of working things out, and had a number of printouts and extra graph that are not needed in the main file 

---
## What You Need to Run 

- **numpy**
  - easily installed with pip3 

- **pandas** 
  - installed with pip3 (pip install pandas) 

- **matplotlib** 
  - installed with pip3 (was already installed)
        - pyplot 
        - patches

- **Ansimarkup** 
  - installed with anaconda with: `conda install -c conda-forge ansimarkup`
  - used to make coloured and bold/italics fonts for the gamification portion of the main file

- **statsmodels** 
    - installed w/ anaconda: `conda install -c conda-forge statsmodels`
    - used in jack-dies to run the logit model on the data

- **sklearn**
  - the Maching Learning package sci-kit learn
  - installed with conda: `conda install -c intel scikit-learn`

---
## References

discussions on the definition of 'child' based on age categories: 
https://www.encyclopedia-titanica.org/community/threads/age-of-majority.4620/

number of trees in random forest suggested between 68-128:
https://www.researchgate.net/publication/230766603_How_Many_Trees_in_a_Random_Forest

survival statistics:
http://www.icyousee.org/titanic.html
