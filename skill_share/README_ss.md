This directroy holds the skill share and practice problems for the various online classes

They are all a work in progress.

---
## Directories 

1. **./Learn-Data-Science-with-Python/:** this is not of my own
   making but the homework and jupyter notebooks of a set of classes
   that i was working through. However, I have stopped aprt way through
   due to some of the material being too beginner
   
2. **./machine_learning/:** this diretory and its sub-directories have
   examples of data sets that can be used for learning about the scikit-learn
   python package.
   a) **./machine_learning/bootcamp_class_1_to_10/:** a whole set of modules
	   downloaded from Dr. Junaid Qazi's skill share bootcamp class. They have a
	   number of useful datasets that I wanted to practice with
   b) **./machine_learning/machine_learning_org/:** Again a downloaded jupyter
	   notebook about machine learning from skillshare, this time from a group, their
	   classes and data sets can be found: https://www.skillshare.com/user/themachinelearning
   c) **./machine_learning/titanic_prediction/:** This is my own directroy that uses
	   well known historical data from the titanic and jupyter noteebook to graph some
	   relationships with survival and predict the survival of a subset of the passengers on the
	   titanic. It has its own readme file, see it for details. 

---
