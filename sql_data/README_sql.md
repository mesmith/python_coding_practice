Repository for python practice using sql (postgres) databases through jupyter notebook 

---
## Directories 


---
## What you need to run 

- This is now run through Jupyter notebook which brings you to your 
    default web browser 
    - open a terminal and navigate to this folder 
    - enter 'Jupyter notebook' and hit enter 
    - this will open your default browser with all the files in the  
        local directory listed
    - click on the jupyter_main.ipynb to open the latest working version 
        of the analysis 

- **Jupyter notebook** 
	- installed using conda

- **postgresql** 
	- installed using conda: conda install -c anaconda postgresql
    
- **SQLAlchemy** 
	- installed using pip: pip install sqlalchemy psycopg2
    - or anaconda: conda install -c anaconda psycopg2 (worked for me)

- **Psycopg2** 
	- installed using conda: conda install -y sqlalchemy psycopg2

- **tabula-py** 
	- takes pdf tables and turns them into pandas dataframe
    - or anaconda: conda install -c conda-forge tabula-py (worked for me)

---

## Git Repository

This is a repo that is located on BitBucket, which runs git. 
Important instructions: 

- git status: shows the status of the files changed in the local dir. 
- git pull: pulls from the repo the lastest chages if working with a 
    group/ multiple computers- this is important to avoid merge errors
- git add <filename> or git add . 
    - <filename> adds that specific file to be committed 
    - . adds all the changed files 
- git commit -m 'message for commit'
    - without the -m then git will use the default editor to force a 
    commit message, which is usually vi/vim (eww)
- git push 
    - pushed all the changes from the local computer to the repo 
