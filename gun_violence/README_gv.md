This  repository is aimed at using the GV_database.csv file, which 
contains Mass Shooting data from the USA starting in 1982

This is a work in progress.

GV.csv is the original gun violence csv downloaded from 
https://www.motherjones.com/politics/2012/12/mass-shootings-mother-jones-full-data/

---
## Files 

1. **Jupyter_main.ipynb:** The main release version of the analysis of
    the mass shooting database (GV_database.csv)
2. **Jupyter_working.ipynb:** a working file that currently has an 
    example of a histogram plot with a gaussian, gamma and exponential 
    best fit lines 

### Files no longer used:
3. **pandas_analysis.py:** this is the main py file, currently just 
    samples of 2 of the 3 types of graphs that will be used (histogram 
    and pie)
4. **UofTmapPractice.py:** originally was a file that was ue to run 
    through practice exercises from the U of Toronto coders website; 
    turned into the working example of mapping all the mass shooting 
    instances over the USA (it shows North America including Hawaii)
5. **katrina_edit.py:** my first attempt to manipulate a map of the USA 
    by playing with the hurricane katrina example- not needed 
6. **hurricane_katrina.py:** this is an example for using cartopy that 
    was pulled from the cartopy documentation 
    (https://scitools.org.uk/cartopy/docs/latest/gallery/index.html)
7. **testcsv.csv:** this is a small csv file that was used to test the 
    import of csv files using csv just to see how it worked, not needed
    for running the program, as it is switched to use pandas 
8. **test_GV.py:** the original file that imports the GV.csv file using 
    csv and was use to learn about python and importing- not used, just 
    for playing with new ideas  
    
---

## What you need to run 

- This is now run through Jupyter notebook which brings you to your 
    default web browser 
    - open a terminal and navigate to this folder 
    - enter 'Jupyter notebook' and hit enter 
    - this will open your default browser with all the files in the  
        local directory listed
    - click on the jupyter_main.ipynb to open the latest working version 
        of the analysis 

- **cartopy**
    - If you need to download cartopy then the best method is to use 
      conda, mini-conda can be downloaded 
      (https://docs.conda.io/en/latest/miniconda.html) and when 
      installed will show a (base) at the beginning of a line in 
      terminal (ubuntu 16.04) you cannot install cartopy with pip3- 
      tried and failed many times bonus of using conda: it downloads 
      all associated packages needed to run cartopy automatically 
    - run: conda install -c conda-forge cartopy
        - crs
        - feature
        - io.shapereader
- **numpy** 
    - easily installed with pip3 
- **shapley** 
    - if using conda this should be downloaded when cartopy is
        - geometry
- **matplotlib** 
    - installed with pip3 (pip install matplotlib)
        - pyplot 
        - patches 
- **pandas**
    - installed with pip3 (pip install pandas) 
    
- **Jupyter notebook** 
    - installed using conda

---

## Git Repository

This is a repo that is located on BitBucket, which runs git. 
Important instructions: 

- git status: shows the status of the files changed in the local dir. 
- git pull: pulls from the repo the lastest chages if working with a 
    group/ multiple computers- this is important to avoid merge errors
- git add <filename> or git add . 
    - <filename> adds that specific file to be committed 
    - . adds all the changed files 
- git commit -m 'message for commit'
    - without the -m then git will use the default editor to force a 
    commit message, which is usually vi/vim (eww)
- git push 
    - pushed all the changes from the local computer to the repo 
