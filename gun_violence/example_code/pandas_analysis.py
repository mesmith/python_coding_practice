#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
##%matplotlib inline

massShooting = pd.read_csv('GV_database.csv')

#print(massShooting.head())
print(massShooting.iloc[-1])

# check that all of the row were imported with the correct number of columns
#for x in range(115):
 #   print(len(massShooting.iloc[x]))

## make some plots 

x = massShooting['year'].tolist()  

#massShooting['year'].hist(bins=37, xlabelsize=12, ylabelsize=12, rwidth=0.9)
# gridlines = False  will take the grid away
#plt.title('Number of Mass Shootings by Year', fontsize = 18)
#plt.ylabel("Number of Mass Shootings", fontsize = 15)
#plt.xlabel("Year", fontsize = 15)
#plt.show()

gender_breakdown = [0,0,0]
gender_label = ['Male', 'Female', 'Both']
# make the gender portion of massShooting a string type to compare with
massShooting['gender'] = massShooting['gender'].astype(str) 
for x in range(0,114): 
    if massShooting.iloc[x,16] == 'Male' or massShooting.iloc[x,16] == 'M':
        gender_breakdown[0] += 1
    elif massShooting.iloc[x,16] == 'Female' or massShooting.iloc[x,16] == 'F': 
        gender_breakdown[1] += 1
    else:
        gender_breakdown[2] += 1 

print(gender_breakdown)

#colors = ["#E13F29", "#D69A80", "#D63B59"]
colors = ['blue', 'red', 'yellow']
plt.pie(
    # using data total)arrests
    gender_breakdown,
    # with the labels being officer names
   # labels=gender_label,
    # with no shadows
    shadow=False,
    # with colors
    colors = colors,
    # with one slide exploded out
    explode=(0.25, 0, 0),
    # with the start angle at 90%
    #startangle=90,
    # with the percent listed as a fraction
    autopct='%1.1f%%',
    )

# View the plot drop above
plt.axis('equal')
plt.title('Perpetrators of Mass Shootings by Gender')
plt.legend(gender_label,loc=1)

# View the plot
plt.tight_layout()
plt.show()

    
