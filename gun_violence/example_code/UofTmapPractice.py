# Default imports
import numpy as np

import matplotlib.pyplot as plt

import cartopy.crs as ccrs
import cartopy.feature as cfeature

import cartopy.io.shapereader as shpreader
import shapely.geometry as sgeom
import matplotlib.patches as mpatches

import pandas as pd

massShooting = pd.read_csv('GV_database.csv')

lon = massShooting['longitude']
lat = massShooting['latitude']

#np.random.seed(1)
#lon = 360 * np.random.rand(100)
#lat = 180 * np.random.rand(100) - 90

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(1, 1, 1,
                     projection=ccrs.PlateCarree())

ax.coastlines()
ax.add_feature(cfeature.BORDERS)
ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.LAKES)
##for extent[start long, end long, start lat, end lat]
ax.set_extent([-175, -60, 15, 90])

def colorize_state(geometry):
        facecolor = (0.9375, 0.9375, 0.859375)
        #if geometry.intersects(track):
         #   facecolor = 'red'
        #elif geometry.intersects(track_buffer):
         #   facecolor = '#FF7E00'
        return {'facecolor': facecolor, 'edgecolor': 'black'}

shapename = 'admin_1_states_provinces_lakes_shp'
states_shp = shpreader.natural_earth(resolution='110m',
                                         category='cultural', name=shapename)
ax.add_geometries(
        shpreader.Reader(states_shp).geometries(),
        ccrs.PlateCarree(),styler=colorize_state, zorder=1)
   
#ax.add_geometries(lon,lat, ccrs.PlateCarree(),
 #                     facecolor='k', edgecolor='k')   
              
ax.scatter(lon, lat,zorder=2,color='Red')
ax.set_title('US States which have incidents of Mass Schootings')

plt.show()
