
# file to read in the mass shooting data and analyze any trends in that data 
# run with : python3.6 ./test_GV.py 

from datetime import datetime 
import csv
# import pdb
# import numpy as np
import plotly.express as pl

with open('GV.csv') as csv_file:
#with open('testcsv.csv') as csv_file: 
        csv_reader = csv.reader(csv_file, delimiter=',') 
        line_count = 0
        for row in csv_reader:
                if line_count == 0:
                        headers = row # first row is the titles of columns
                        gun_data = [headers]
                       # print(headers)
                        line_count += 2 #actual row number when looking at file
                else:
                        current_row = row
                        if len(row) == 24:
                                gun_data.append(current_row)
                        elif len(row) < 24:
                                print(len(row))
                                print('line', line_count)
                                compound_row = [current_row[0]]
                                for z in range(1,18):
                                        compound_row += [current_row[z]]
                                null = '-'
                                compound_row += [null]
                                compound_row += [null] 
                                for z in range(18,22):
                                        compound_row += [current_row[z]]
                                gun_data.append(compound_row)
                        else:
                                print(len(row))
                                print('line', line_count)
                        line_count += 1

#for z in range(1, len(gun_data[14])):
#        print(z, "+", gun_data[14][z])

#print(len(headers))
print(len(gun_data))
#print(gun_data[24])
# change the relevant columns into floats, integers and datetime for data processing! 

for x in range(115): #rows 115 rows in the csv file
     #   print('current row is:', x)
        if x == 0: 
                continue 
        else: 
                for y in range(len(headers)+1): #columns
                        if y==4 or y==5 or y==6 or y==8 or y==23:
                                #print(x)
                                gun_data[x][y] = int(gun_data[x][y])
                        elif y==20 or y==21:
                                gun_data[x][y] = float(gun_data[x][y])
                        elif y == 2:
                                string = gun_data[x][y]
                                gun_data[x][y] = datetime.strptime(string, '%m/%d/%Y')
                        else: 
                                continue

# separate the location and description into smaller strings
for x in range(115): ## y will be set to 1 and 3(will be +1) 
         if x == 0:
                 # location split (y == 1) of header
                 string = ['city','state']
                 gun_data[x] = gun_data[x][:1] + string + gun_data[x][2:] # headers section addition
                
                 #summary split (y == 4 - the new #) of header
                 string = ['perp'] # age of perp is already there 
                 gun_data[x] = gun_data[x][:4] + string + gun_data[x][4:]
         else:
                 # location split of all the rows
                 string =  gun_data[x][1].split(',',2) # split the 2nd column into 2 along ','
                 gun_data[x] = gun_data[x][:1] + string + gun_data[x][2:] #insert into gun_data

                 # summary split (y == 4) of all the rows 
                 string = gun_data[x][4].split(',',1) # split the 4th column into 3 along ','
                 gun_data[x] = gun_data[x][:4] + string + gun_data[x][5:]

# now that the data is loaded properly, it is time to analyze and graph it


first_fig = pl.bar(gun_data, x='year', y='fatalities')
fig.show()
                        
                





