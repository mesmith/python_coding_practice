  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    28.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.227901532439695        35.252476202681969       nm 
  Mean penetration     =    12.318450045125914        9.2617221542580133       nm 
  Mean radial distance =    8.9746333420766380        8.1231090302153941       nm 
  Mean axial distance  =    6.9215493070716105        6.5640627682275303       nm 

  Final event probability : 
       energy thermalization =    3.2900000000000004E-003
       electronic excitation =    3.4100000000000003E-003
       capture (energetic)   =   0.36788000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8077200000000002     
       vib. excitation, v2   =   0.28462000000000004     
       vib. excitation, v13  =   0.70093000000000005     
       vib. excitation, vr   =   0.21770000000000000     
       Phonon excitation, t1 =    4.2690000000000006E-002
       Phonon excitation, t2 =    1.8582900000000002     
       Phonon excitation, l1 =    1.2067900000000000     
       Phonon excitation, l2 =    3.7523500000000003     
       electronic excitation =    1.2678300000000000     
               a) ionization =    1.0316200000000000     
               b) excitation =   0.23621000000000003     
       electron capture      =   0.99330000000000007     

