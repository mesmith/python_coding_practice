  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    10.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    4.3274425305916289        7.3880508626146826       nm 
  Mean penetration     =    2.9060000965470998        3.0792389264968847       nm 
  Mean radial distance =    1.1902481348377900        2.4263339198170626       nm 
  Mean axial distance  =    2.2502026010122678        2.3578274468339200       nm 

  Final event probability : 
       energy thermalization =    9.5000000000000011E-004
       electronic excitation =    2.3400000000000001E-003
       capture (energetic)   =   0.91119000000000006     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.30534000000000000     
       vib. excitation, v2   =    4.0210000000000003E-002
       vib. excitation, v13  =   0.12983000000000000     
       vib. excitation, vr   =    3.9490000000000004E-002
       Phonon excitation, t1 =    3.2600000000000003E-003
       Phonon excitation, t2 =   0.15287000000000001     
       Phonon excitation, l1 =   0.15051000000000000     
       Phonon excitation, l2 =   0.39221000000000006     
       electronic excitation =    8.8520000000000001E-002
               a) ionization =    1.4060000000000001E-002
               b) excitation =    7.4460000000000012E-002
       electron capture      =   0.99671000000000010     

