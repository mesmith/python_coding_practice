  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    34.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.139950377353202        34.058323425743801       nm 
  Mean penetration     =    12.489107468032380        9.1979040244484889       nm 
  Mean radial distance =    9.1707196567968339        8.0138451106781901       nm 
  Mean axial distance  =    6.9754266946123185        6.6029428089008091       nm 

  Final event probability : 
       energy thermalization =    4.2000000000000006E-003
       electronic excitation =    2.2000000000000001E-003
       capture (energetic)   =   0.33909000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8362100000000003     
       vib. excitation, v2   =   0.29405000000000003     
       vib. excitation, v13  =   0.69026000000000010     
       vib. excitation, vr   =   0.19362000000000001     
       Phonon excitation, t1 =    6.1480000000000007E-002
       Phonon excitation, t2 =    1.7794400000000001     
       Phonon excitation, l1 =    1.2182800000000000     
       Phonon excitation, l2 =    3.5827000000000004     
       electronic excitation =    1.5365700000000002     
               a) ionization =    1.2238300000000002     
               b) excitation =   0.31274000000000002     
       electron capture      =   0.99360000000000004     

