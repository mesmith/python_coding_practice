  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    22.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    30.161162645115496        28.176852543218743       nm 
  Mean penetration     =    11.157472677179998        8.3823286453761394       nm 
  Mean radial distance =    7.9364511238635700        7.2952899090849597       nm 
  Mean axial distance  =    6.4496525004682974        6.0783304768037745       nm 

  Final event probability : 
       energy thermalization =    5.2100000000000002E-003
       electronic excitation =    7.0000000000000007E-005
       capture (energetic)   =   0.17177000000000001     
       reactive capture      =    2.6100000000000003E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.3045000000000000     
       vib. excitation, v2   =   0.22956000000000001     
       vib. excitation, v13  =   0.55652000000000001     
       vib. excitation, vr   =   0.15456000000000000     
       Phonon excitation, t1 =    3.4750000000000003E-002
       Phonon excitation, t2 =    1.6079200000000002     
       Phonon excitation, l1 =    1.0224000000000000     
       Phonon excitation, l2 =    3.2866800000000005     
       electronic excitation =    1.0867900000000001     
               a) ionization =   0.90362000000000009     
               b) excitation =   0.18317000000000003     
       electron capture      =   0.99211000000000005     

