  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    85.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.641972477212086        34.689076811041495       nm 
  Mean penetration     =    13.795585277486785        9.1873102556913473       nm 
  Mean radial distance =    10.223667690689791        8.0851269626451536       nm 
  Mean axial distance  =    7.6198394049979532        6.8388764627702896       nm 

  Final event probability : 
       energy thermalization =    4.8100000000000000E-003
       electronic excitation =    3.8000000000000002E-004
       capture (energetic)   =   0.30651000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9024700000000001     
       vib. excitation, v2   =   0.32515000000000005     
       vib. excitation, v13  =   0.71028000000000002     
       vib. excitation, vr   =   0.19807000000000002     
       Phonon excitation, t1 =    7.8060000000000004E-002
       Phonon excitation, t2 =    2.0011700000000001     
       Phonon excitation, l1 =    1.4068400000000001     
       Phonon excitation, l2 =    3.8542900000000002     
       electronic excitation =    2.9747200000000000     
               a) ionization =    2.6675200000000001     
               b) excitation =   0.30720000000000003     
       electron capture      =   0.99481000000000008     

