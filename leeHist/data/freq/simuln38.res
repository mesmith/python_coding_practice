  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    38.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.120485809482098        34.343828448526985       nm 
  Mean penetration     =    12.734812030777022        9.2104610327350489       nm 
  Mean radial distance =    9.4116005538332672        8.0761573725227827       nm 
  Mean axial distance  =    7.0514144213240924        6.5941672455912448       nm 

  Final event probability : 
       energy thermalization =    4.5800000000000007E-003
       electronic excitation =    1.5000000000000000E-003
       capture (energetic)   =   0.29329000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9018200000000003     
       vib. excitation, v2   =   0.29835000000000000     
       vib. excitation, v13  =   0.69021000000000010     
       vib. excitation, vr   =   0.19056000000000001     
       Phonon excitation, t1 =    6.9550000000000001E-002
       Phonon excitation, t2 =    1.8466300000000002     
       Phonon excitation, l1 =    1.2402700000000002     
       Phonon excitation, l2 =    3.7049300000000005     
       electronic excitation =    1.7009100000000001     
               a) ionization =    1.3881600000000001     
               b) excitation =   0.31275000000000003     
       electron capture      =   0.99392000000000014     

