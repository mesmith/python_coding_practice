  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    77.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.766629905397657        34.697159254003566       nm 
  Mean penetration     =    13.533892798960412        9.1413456144362701       nm 
  Mean radial distance =    10.036836266486096        8.0116012714924327       nm 
  Mean axial distance  =    7.4774391595171004        6.7745494594410172       nm 

  Final event probability : 
       energy thermalization =    4.3900000000000007E-003
       electronic excitation =    5.3000000000000009E-004
       capture (energetic)   =   0.30776000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8750100000000001     
       vib. excitation, v2   =   0.31799000000000005     
       vib. excitation, v13  =   0.70383000000000007     
       vib. excitation, vr   =   0.18988000000000002     
       Phonon excitation, t1 =    7.7460000000000001E-002
       Phonon excitation, t2 =    1.9573000000000003     
       Phonon excitation, l1 =    1.3735300000000001     
       Phonon excitation, l2 =    3.8192700000000004     
       electronic excitation =    2.7794200000000004     
               a) ionization =    2.4742900000000003     
               b) excitation =   0.30513000000000001     
       electron capture      =   0.99508000000000008     

