  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    11.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    9.3538570006426038        9.5745077528580396       nm 
  Mean penetration     =    5.5749771719771664        4.6439384529227095       nm 
  Mean radial distance =    2.8719084689401693        3.6906071145278441       nm 
  Mean axial distance  =    4.0274049182420644        3.8155085269655267       nm 

  Final event probability : 
       energy thermalization =    1.4900000000000002E-003
       electronic excitation =    3.1300000000000004E-003
       capture (energetic)   =   0.82151000000000007     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.71335000000000004     
       vib. excitation, v2   =    7.5730000000000006E-002
       vib. excitation, v13  =   0.22698000000000002     
       vib. excitation, vr   =    5.2730000000000006E-002
       Phonon excitation, t1 =    1.5600000000000002E-003
       Phonon excitation, t2 =   0.32835000000000003     
       Phonon excitation, l1 =   0.31120000000000003     
       Phonon excitation, l2 =   0.80746000000000007     
       electronic excitation =   0.17857000000000001     
               a) ionization =    4.6010000000000002E-002
               b) excitation =   0.13256000000000001     
       electron capture      =   0.99538000000000004     

