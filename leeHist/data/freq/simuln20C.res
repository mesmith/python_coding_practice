  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    20.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    29.535522892504122        28.230104636832944       nm 
  Mean penetration     =    10.966304780489894        8.3243045385149266       nm 
  Mean radial distance =    7.7534972869413830        7.2374584360965697       nm 
  Mean axial distance  =    6.3657316275177269        6.0443215204123195       nm 

  Final event probability : 
       energy thermalization =    6.3500000000000006E-003
       electronic excitation =    9.0000000000000006E-005
       capture (energetic)   =   0.15246000000000001     
       reactive capture      =    3.0100000000000001E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.3202700000000003     
       vib. excitation, v2   =   0.23191000000000003     
       vib. excitation, v13  =   0.56045000000000000     
       vib. excitation, vr   =   0.14486000000000002     
       Phonon excitation, t1 =    4.0300000000000002E-002
       Phonon excitation, t2 =    1.5642700000000000     
       Phonon excitation, l1 =    1.0020100000000001     
       Phonon excitation, l2 =    3.2003300000000001     
       electronic excitation =    1.0659800000000001     
               a) ionization =   0.83876000000000006     
               b) excitation =   0.22722000000000001     
       electron capture      =   0.99052000000000007     

