  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    17.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    34.169379081311639        30.583305530919933       nm 
  Mean penetration     =    12.084892561222864        8.9813362636119187       nm 
  Mean radial distance =    8.6440238401286607        7.8356905108404886       nm 
  Mean axial distance  =    6.9231237017147444        6.5316302388251719       nm 

  Final event probability : 
       energy thermalization =    6.6400000000000009E-003
       electronic excitation =    8.0000000000000007E-005
       capture (energetic)   =   0.11532000000000001     
       reactive capture      =    3.6300000000000004E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9067000000000003     
       vib. excitation, v2   =   0.27908000000000005     
       vib. excitation, v13  =   0.64592000000000005     
       vib. excitation, vr   =   0.15251000000000001     
       Phonon excitation, t1 =    6.0160000000000005E-002
       Phonon excitation, t2 =    1.6908200000000002     
       Phonon excitation, l1 =    1.1183200000000000     
       Phonon excitation, l2 =    3.4847500000000005     
       electronic excitation =    1.0418500000000002     
               a) ionization =   0.63445000000000007     
               b) excitation =   0.40740000000000004     
       electron capture      =   0.98963000000000012     

