  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    47.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.806891814860400        34.547209947078663       nm 
  Mean penetration     =    12.995695167060102        9.1905086279976000       nm 
  Mean radial distance =    9.5582681775005032        8.0722041137562499       nm 
  Mean axial distance  =    7.2522396494698365        6.6511346382361900       nm 

  Final event probability : 
       energy thermalization =    4.1800000000000006E-003
       electronic excitation =    1.0000000000000000E-003
       capture (energetic)   =   0.29923000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8570700000000002     
       vib. excitation, v2   =   0.29656000000000005     
       vib. excitation, v13  =   0.68479000000000001     
       vib. excitation, vr   =   0.19624000000000003     
       Phonon excitation, t1 =    7.5950000000000004E-002
       Phonon excitation, t2 =    1.8746600000000002     
       Phonon excitation, l1 =    1.2696800000000001     
       Phonon excitation, l2 =    3.7372900000000002     
       electronic excitation =    1.9591200000000002     
               a) ionization =    1.6784100000000002     
               b) excitation =   0.28071000000000002     
       electron capture      =   0.99482000000000004     

