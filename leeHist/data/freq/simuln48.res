  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    48.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.339108835310270        34.686204519397648       nm 
  Mean penetration     =    13.121889656872034        9.2329727973236455       nm 
  Mean radial distance =    9.6389543945787892        8.0598976570570215       nm 
  Mean axial distance  =    7.3381615513894340        6.7610478349107623       nm 

  Final event probability : 
       energy thermalization =    4.5100000000000001E-003
       electronic excitation =    9.4000000000000008E-004
       capture (energetic)   =   0.30397000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8860600000000001     
       vib. excitation, v2   =   0.30640000000000001     
       vib. excitation, v13  =   0.69214000000000009     
       vib. excitation, vr   =   0.19829000000000002     
       Phonon excitation, t1 =    7.4920000000000000E-002
       Phonon excitation, t2 =    1.9056200000000001     
       Phonon excitation, l1 =    1.2811100000000002     
       Phonon excitation, l2 =    3.7686900000000003     
       electronic excitation =    1.9879900000000001     
               a) ionization =    1.7034800000000001     
               b) excitation =   0.28451000000000004     
       electron capture      =   0.99455000000000005     

