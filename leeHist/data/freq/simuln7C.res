  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    7.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    44.174302785967143        38.553716254369590       nm 
  Mean penetration     =    13.360575934348098        10.414204566771406       nm 
  Mean radial distance =    9.8005181530780696        9.1947608837423598       nm 
  Mean axial distance  =    7.3292738197429994        7.2559360218555931       nm 

  Final event probability : 
       energy thermalization =    1.0000000000000001E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.54208000000000001     
       reactive capture      =    3.5800000000000003E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.9495900000000002     
       vib. excitation, v2   =   0.36780000000000002     
       vib. excitation, v13  =   0.98610000000000009     
       vib. excitation, vr   =   0.35397000000000001     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.4420100000000002     
       Phonon excitation, l1 =    1.3524700000000001     
       Phonon excitation, l2 =    4.9502800000000002     
       electronic excitation =   0.16965000000000002     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.16965000000000002     
       electron capture      =   0.99641000000000013     

