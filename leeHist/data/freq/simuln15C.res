  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    15.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.175406478123939        27.798743888708739       nm 
  Mean penetration     =    13.597318432536342        9.1347792499338745       nm 
  Mean radial distance =    9.7656207268122088        8.0011774140885343       nm 
  Mean axial distance  =    7.7666374619777461        6.9731208826000692       nm 

  Final event probability : 
       energy thermalization =    6.4500000000000009E-003
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.13917000000000002     
       reactive capture      =    3.8300000000000005E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.2768800000000002     
       vib. excitation, v2   =   0.30434000000000000     
       vib. excitation, v13  =   0.71995000000000009     
       vib. excitation, vr   =   0.16489000000000001     
       Phonon excitation, t1 =    8.4130000000000010E-002
       Phonon excitation, t2 =    1.6918900000000001     
       Phonon excitation, l1 =    1.2025700000000001     
       Phonon excitation, l2 =    3.5925200000000004     
       electronic excitation =   0.94124000000000008     
               a) ionization =   0.42473000000000005     
               b) excitation =   0.51651000000000002     
       electron capture      =   0.98970000000000014     

