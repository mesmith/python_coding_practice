  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    84.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.532565704649329        34.870551088820186       nm 
  Mean penetration     =    13.749961168763122        9.2380406742850028       nm 
  Mean radial distance =    10.211640834757768        8.1201416290150910       nm 
  Mean axial distance  =    7.5722857139801150        6.8446335252321511       nm 

  Final event probability : 
       energy thermalization =    4.6900000000000006E-003
       electronic excitation =    4.2000000000000002E-004
       capture (energetic)   =   0.30409000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9016500000000001     
       vib. excitation, v2   =   0.32700000000000001     
       vib. excitation, v13  =   0.71076000000000006     
       vib. excitation, vr   =   0.19532000000000002     
       Phonon excitation, t1 =    7.7330000000000010E-002
       Phonon excitation, t2 =    1.9912900000000002     
       Phonon excitation, l1 =    1.4037900000000001     
       Phonon excitation, l2 =    3.8484700000000003     
       electronic excitation =    2.9536600000000002     
               a) ionization =    2.6428100000000003     
               b) excitation =   0.31085000000000002     
       electron capture      =   0.99489000000000005     

