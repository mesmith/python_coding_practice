  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    10.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    9.5307171115712244        19.442699631972651       nm 
  Mean penetration     =    4.4090849083553936        5.4881280324756352       nm 
  Mean radial distance =    2.5228033478343415        4.6755287226875080       nm 
  Mean axial distance  =    2.9229032705325872        3.5764661234970712       nm 

  Final event probability : 
       energy thermalization =    3.2900000000000004E-003
       electronic excitation =    1.2000000000000002E-004
       capture (energetic)   =   0.82606000000000002     
       reactive capture      =    5.3300000000000005E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.76256000000000002     
       vib. excitation, v2   =    9.6960000000000005E-002
       vib. excitation, v13  =   0.31071000000000004     
       vib. excitation, vr   =   0.10186000000000001     
       Phonon excitation, t1 =    8.2000000000000007E-003
       Phonon excitation, t2 =   0.42458000000000001     
       Phonon excitation, l1 =   0.34999000000000002     
       Phonon excitation, l2 =   0.99566000000000010     
       electronic excitation =   0.15459000000000001     
               a) ionization =    1.6270000000000000E-002
               b) excitation =   0.13832000000000000     
       electron capture      =   0.99123000000000006     

