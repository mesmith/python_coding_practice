  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    70.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.744781836318630        34.779727284213671       nm 
  Mean penetration     =    13.520038096427321        9.1976641071877463       nm 
  Mean radial distance =    9.9908141094652425        8.0726571506108016       nm 
  Mean axial distance  =    7.4987209031844717        6.7951070582033450       nm 

  Final event probability : 
       energy thermalization =    4.4400000000000004E-003
       electronic excitation =    6.5000000000000008E-004
       capture (energetic)   =   0.30500000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9065000000000003     
       vib. excitation, v2   =   0.32184000000000001     
       vib. excitation, v13  =   0.70830000000000004     
       vib. excitation, vr   =   0.19645000000000001     
       Phonon excitation, t1 =    8.0860000000000001E-002
       Phonon excitation, t2 =    1.9648800000000002     
       Phonon excitation, l1 =    1.3703600000000000     
       Phonon excitation, l2 =    3.8228300000000002     
       electronic excitation =    2.6063800000000001     
               a) ionization =    2.3034600000000003     
               b) excitation =   0.30292000000000002     
       electron capture      =   0.99491000000000007     

