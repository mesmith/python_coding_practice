  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    61.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.150779415484493        34.586165362980239       nm 
  Mean penetration     =    13.428917026967337        9.1841081362192014       nm 
  Mean radial distance =    9.8620141425928232        8.0506598311202016       nm 
  Mean axial distance  =    7.5022440636556773        6.8064338767944257       nm 

  Final event probability : 
       energy thermalization =    4.4500000000000000E-003
       electronic excitation =    7.1000000000000002E-004
       capture (energetic)   =   0.30717000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8925600000000000     
       vib. excitation, v2   =   0.31269000000000002     
       vib. excitation, v13  =   0.69712000000000007     
       vib. excitation, vr   =   0.19376000000000002     
       Phonon excitation, t1 =    8.5620000000000002E-002
       Phonon excitation, t2 =    1.9168400000000001     
       Phonon excitation, l1 =    1.3202600000000002     
       Phonon excitation, l2 =    3.7559800000000001     
       electronic excitation =    2.3651200000000001     
               a) ionization =    2.0637000000000003     
               b) excitation =   0.30142000000000002     
       electron capture      =   0.99484000000000006     

