  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    56.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.956765946173348        34.755071775233553       nm 
  Mean penetration     =    13.282177829191481        9.1823843308136830       nm 
  Mean radial distance =    9.7374578772551086        8.0308392045162709       nm 
  Mean axial distance  =    7.4482452162897586        6.7781714970144069       nm 

  Final event probability : 
       energy thermalization =    4.2400000000000007E-003
       electronic excitation =    8.8000000000000003E-004
       capture (energetic)   =   0.31155000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9020600000000001     
       vib. excitation, v2   =   0.31118000000000001     
       vib. excitation, v13  =   0.70490000000000008     
       vib. excitation, vr   =   0.19691000000000003     
       Phonon excitation, t1 =    8.3280000000000007E-002
       Phonon excitation, t2 =    1.9191300000000002     
       Phonon excitation, l1 =    1.3123300000000000     
       Phonon excitation, l2 =    3.7612900000000002     
       electronic excitation =    2.2198500000000001     
               a) ionization =    1.9254400000000003     
               b) excitation =   0.29441000000000001     
       electron capture      =   0.99488000000000010     

