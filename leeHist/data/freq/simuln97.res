  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    97.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.550257699354965        34.887078583200847       nm 
  Mean penetration     =    14.037909246020394        9.1425233133905923       nm 
  Mean radial distance =    10.367310051127157        8.0537780388350981       nm 
  Mean axial distance  =    7.7955620838584485        6.8944457309544340       nm 

  Final event probability : 
       energy thermalization =    4.4500000000000000E-003
       electronic excitation =    4.4000000000000002E-004
       capture (energetic)   =   0.30715000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9172300000000004     
       vib. excitation, v2   =   0.33596000000000004     
       vib. excitation, v13  =   0.71444000000000007     
       vib. excitation, vr   =   0.19395000000000001     
       Phonon excitation, t1 =    7.7690000000000009E-002
       Phonon excitation, t2 =    2.0321700000000003     
       Phonon excitation, l1 =    1.4419100000000000     
       Phonon excitation, l2 =    3.8884400000000001     
       electronic excitation =    3.2630900000000000     
               a) ionization =    2.9472400000000003     
               b) excitation =   0.31585000000000002     
       electron capture      =   0.99511000000000005     

