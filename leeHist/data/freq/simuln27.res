  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    27.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.503098712629431        35.552725934275479       nm 
  Mean penetration     =    12.373288994536932        9.3188507409404426       nm 
  Mean radial distance =    9.0167865973014099        8.1496191552427497       nm 
  Mean axial distance  =    6.9475157352678902        6.6296720782923204       nm 

  Final event probability : 
       energy thermalization =    3.2800000000000004E-003
       electronic excitation =    3.8400000000000005E-003
       capture (energetic)   =   0.34967000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8212400000000004     
       vib. excitation, v2   =   0.28715000000000002     
       vib. excitation, v13  =   0.69966000000000006     
       vib. excitation, vr   =   0.21470000000000003     
       Phonon excitation, t1 =    3.9700000000000006E-002
       Phonon excitation, t2 =    1.8897100000000002     
       Phonon excitation, l1 =    1.2111600000000000     
       Phonon excitation, l2 =    3.8244200000000004     
       electronic excitation =    1.2385800000000000     
               a) ionization =    1.0146000000000002     
               b) excitation =   0.22398000000000001     
       electron capture      =   0.99288000000000010     

