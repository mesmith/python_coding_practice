  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    12.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.925278761526016        24.464618711650051       nm 
  Mean penetration     =    13.881333351763059        8.8028095343475279       nm 
  Mean radial distance =    9.8691939506805060        7.6645650114234867       nm 
  Mean axial distance  =    8.0331968320627922        7.0357710258091020       nm 

  Final event probability : 
       energy thermalization =    5.0700000000000007E-003
       electronic excitation =    5.3100000000000005E-003
       capture (energetic)   =   0.28068000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9960200000000001     
       vib. excitation, v2   =   0.29596000000000000     
       vib. excitation, v13  =   0.78441000000000005     
       vib. excitation, vr   =   0.17365000000000003     
       Phonon excitation, t1 =    3.2740000000000005E-002
       Phonon excitation, t2 =    1.4328500000000002     
       Phonon excitation, l1 =    1.1894900000000002     
       Phonon excitation, l2 =    3.2718100000000003     
       electronic excitation =   0.72658000000000011     
               a) ionization =   0.22697000000000001     
               b) excitation =   0.49961000000000005     
       electron capture      =   0.98962000000000006     

