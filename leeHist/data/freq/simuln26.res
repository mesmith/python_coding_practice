  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    26.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.318136488564299        35.369550091254581       nm 
  Mean penetration     =    12.320161429479260        9.2756604059203127       nm 
  Mean radial distance =    8.9310875993044885        8.0978295200906487       nm 
  Mean axial distance  =    6.9648006250987358        6.6314883113903011       nm 

  Final event probability : 
       energy thermalization =    3.7400000000000003E-003
       electronic excitation =    4.8200000000000005E-003
       capture (energetic)   =   0.32209000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7972800000000002     
       vib. excitation, v2   =   0.27769000000000005     
       vib. excitation, v13  =   0.69326000000000010     
       vib. excitation, vr   =   0.21584000000000000     
       Phonon excitation, t1 =    3.8440000000000002E-002
       Phonon excitation, t2 =    1.8981900000000003     
       Phonon excitation, l1 =    1.2067100000000002     
       Phonon excitation, l2 =    3.8403800000000001     
       electronic excitation =    1.2132400000000001     
               a) ionization =   0.99521000000000004     
               b) excitation =   0.21803000000000003     
       electron capture      =   0.99144000000000010     

