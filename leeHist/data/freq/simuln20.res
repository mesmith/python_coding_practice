  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    20.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    31.561592906812205        31.835664617533979       nm 
  Mean penetration     =    11.308561218633765        8.7751593592697468       nm 
  Mean radial distance =    8.0257330484813743        7.6217682382879888       nm 
  Mean axial distance  =    6.5258758324744770        6.3084214500640901       nm 

  Final event probability : 
       energy thermalization =    4.9500000000000004E-003
       electronic excitation =    9.2000000000000016E-003
       capture (energetic)   =   0.18794000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.4448600000000003     
       vib. excitation, v2   =   0.24708000000000002     
       vib. excitation, v13  =   0.59637000000000007     
       vib. excitation, vr   =   0.16436000000000001     
       Phonon excitation, t1 =    3.8120000000000001E-002
       Phonon excitation, t2 =    1.6699500000000000     
       Phonon excitation, l1 =    1.0652800000000000     
       Phonon excitation, l2 =    3.4062500000000004     
       electronic excitation =    1.0885400000000001     
               a) ionization =   0.87897000000000003     
               b) excitation =   0.20957000000000001     
       electron capture      =   0.98585000000000012     

