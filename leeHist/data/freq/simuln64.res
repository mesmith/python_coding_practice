  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    64.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.560814338181892        34.722792050245310       nm 
  Mean penetration     =    13.500913574664127        9.2061425212888519       nm 
  Mean radial distance =    9.9353595429991604        8.0936473226379686       nm 
  Mean axial distance  =    7.5371939382992874        6.7823254697327213       nm 

  Final event probability : 
       energy thermalization =    4.4900000000000001E-003
       electronic excitation =    6.1000000000000008E-004
       capture (energetic)   =   0.30608000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8979000000000004     
       vib. excitation, v2   =   0.31772000000000000     
       vib. excitation, v13  =   0.70318000000000003     
       vib. excitation, vr   =   0.19537000000000002     
       Phonon excitation, t1 =    8.6180000000000007E-002
       Phonon excitation, t2 =    1.9415400000000003     
       Phonon excitation, l1 =    1.3384100000000001     
       Phonon excitation, l2 =    3.7952600000000003     
       electronic excitation =    2.4468000000000001     
               a) ionization =    2.1436800000000003     
               b) excitation =   0.30312000000000000     
       electron capture      =   0.99490000000000012     

