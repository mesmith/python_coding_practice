  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    44.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.502296769030600        34.433102233296182       nm 
  Mean penetration     =    12.866209055325807        9.1845607870623027       nm 
  Mean radial distance =    9.4728898652324531        8.0144368956086556       nm 
  Mean axial distance  =    7.1670247027679501        6.6755080753444869       nm 

  Final event probability : 
       energy thermalization =    4.6500000000000005E-003
       electronic excitation =    1.0700000000000000E-003
       capture (energetic)   =   0.28876000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8679600000000001     
       vib. excitation, v2   =   0.29743000000000003     
       vib. excitation, v13  =   0.67747000000000002     
       vib. excitation, vr   =   0.19214000000000001     
       Phonon excitation, t1 =    7.3750000000000010E-002
       Phonon excitation, t2 =    1.8721200000000002     
       Phonon excitation, l1 =    1.2641900000000001     
       Phonon excitation, l2 =    3.7278900000000004     
       electronic excitation =    1.8831700000000002     
               a) ionization =    1.5948600000000002     
               b) excitation =   0.28831000000000001     
       electron capture      =   0.99428000000000005     

