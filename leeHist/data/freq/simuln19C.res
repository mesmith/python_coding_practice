  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    19.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    30.185181588146420        28.913279461491395       nm 
  Mean penetration     =    11.016742405550000        8.4265628828054240       nm 
  Mean radial distance =    7.8157416898536338        7.3271928850034733       nm 
  Mean axial distance  =    6.3719121759618327        6.0828230891080670       nm 

  Final event probability : 
       energy thermalization =    6.2400000000000008E-003
       electronic excitation =    1.4000000000000001E-004
       capture (energetic)   =   0.13798000000000002     
       reactive capture      =    3.3800000000000002E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.4475400000000000     
       vib. excitation, v2   =   0.24524000000000001     
       vib. excitation, v13  =   0.57829000000000008     
       vib. excitation, vr   =   0.14368000000000000     
       Phonon excitation, t1 =    4.3590000000000004E-002
       Phonon excitation, t2 =    1.5920000000000001     
       Phonon excitation, l1 =    1.0196900000000000     
       Phonon excitation, l2 =    3.2458200000000001     
       electronic excitation =    1.0609000000000002     
               a) ionization =   0.78967000000000009     
               b) excitation =   0.27123000000000003     
       electron capture      =   0.99022000000000010     

