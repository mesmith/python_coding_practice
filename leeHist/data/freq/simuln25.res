  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    25.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.210113340822680        35.262008439130703       nm 
  Mean penetration     =    12.288378335714702        9.3184738003917538       nm 
  Mean radial distance =    8.9051580903718968        8.1182828178562012       nm 
  Mean axial distance  =    6.9413883109122354        6.6668559218092858       nm 

  Final event probability : 
       energy thermalization =    3.1400000000000004E-003
       electronic excitation =    5.6300000000000005E-003
       capture (energetic)   =   0.28988000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7844500000000001     
       vib. excitation, v2   =   0.27779000000000004     
       vib. excitation, v13  =   0.67753000000000008     
       vib. excitation, vr   =   0.21102000000000001     
       Phonon excitation, t1 =    3.6650000000000002E-002
       Phonon excitation, t2 =    1.8989200000000002     
       Phonon excitation, l1 =    1.2010700000000001     
       Phonon excitation, l2 =    3.8543100000000003     
       electronic excitation =    1.1890300000000000     
               a) ionization =   0.97875000000000012     
               b) excitation =   0.21028000000000002     
       electron capture      =   0.99123000000000006     

