  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    76.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.778696925130539        34.654649412193059       nm 
  Mean penetration     =    13.588014796979506        9.1948499829181607       nm 
  Mean radial distance =    10.072853104012289        8.0507498554455097       nm 
  Mean axial distance  =    7.5043257473900535        6.8255083706928614       nm 

  Final event probability : 
       energy thermalization =    4.7900000000000000E-003
       electronic excitation =    4.3000000000000004E-004
       capture (energetic)   =   0.30931000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8846700000000003     
       vib. excitation, v2   =   0.32098000000000004     
       vib. excitation, v13  =   0.70058000000000009     
       vib. excitation, vr   =   0.19107000000000002     
       Phonon excitation, t1 =    7.6670000000000002E-002
       Phonon excitation, t2 =    1.9632500000000002     
       Phonon excitation, l1 =    1.3744400000000001     
       Phonon excitation, l2 =    3.8154700000000004     
       electronic excitation =    2.7544300000000002     
               a) ionization =    2.4513800000000003     
               b) excitation =   0.30305000000000004     
       electron capture      =   0.99478000000000011     

