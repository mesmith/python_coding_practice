  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    88.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.673212735716326        34.518739451699894       nm 
  Mean penetration     =    13.874168587514744        9.2243092989834814       nm 
  Mean radial distance =    10.234944066942639        8.1000527473775925       nm 
  Mean axial distance  =    7.7242659605752335        6.8957390354221948       nm 

  Final event probability : 
       energy thermalization =    4.4000000000000003E-003
       electronic excitation =    3.4000000000000002E-004
       capture (energetic)   =   0.30677000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8869300000000004     
       vib. excitation, v2   =   0.32972000000000001     
       vib. excitation, v13  =   0.71083000000000007     
       vib. excitation, vr   =   0.19530000000000003     
       Phonon excitation, t1 =    7.8820000000000001E-002
       Phonon excitation, t2 =    1.9899700000000002     
       Phonon excitation, l1 =    1.4067700000000001     
       Phonon excitation, l2 =    3.8204500000000001     
       electronic excitation =    3.0509200000000001     
               a) ionization =    2.7395300000000002     
               b) excitation =   0.31139000000000000     
       electron capture      =   0.99526000000000003     

