  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    90.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.144012738723504        34.775967980506913       nm 
  Mean penetration     =    13.941894127571826        9.1748772949457873       nm 
  Mean radial distance =    10.319552459834531        8.0643804503605345       nm 
  Mean axial distance  =    7.7229746221198354        6.8835349355101796       nm 

  Final event probability : 
       energy thermalization =    4.6900000000000006E-003
       electronic excitation =    3.7000000000000005E-004
       capture (energetic)   =   0.30485000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9146100000000001     
       vib. excitation, v2   =   0.33010000000000000     
       vib. excitation, v13  =   0.72120000000000006     
       vib. excitation, vr   =   0.19663000000000003     
       Phonon excitation, t1 =    8.0810000000000007E-002
       Phonon excitation, t2 =    2.0177000000000000     
       Phonon excitation, l1 =    1.4198100000000000     
       Phonon excitation, l2 =    3.8697600000000003     
       electronic excitation =    3.1007800000000003     
               a) ionization =    2.7870700000000004     
               b) excitation =   0.31371000000000004     
       electron capture      =   0.99494000000000005     

