  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    43.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.439865558405678        34.578514379877078       nm 
  Mean penetration     =    12.844981668599242        9.1923003023630407       nm 
  Mean radial distance =    9.4621951173466190        8.0401951333768977       nm 
  Mean axial distance  =    7.1462694783412255        6.6516837896046219       nm 

  Final event probability : 
       energy thermalization =    5.0100000000000006E-003
       electronic excitation =    1.1400000000000002E-003
       capture (energetic)   =   0.28373000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8671300000000004     
       vib. excitation, v2   =   0.29929000000000000     
       vib. excitation, v13  =   0.68040000000000000     
       vib. excitation, vr   =   0.18858000000000003     
       Phonon excitation, t1 =    7.2820000000000010E-002
       Phonon excitation, t2 =    1.8642400000000001     
       Phonon excitation, l1 =    1.2577400000000001     
       Phonon excitation, l2 =    3.7188100000000004     
       electronic excitation =    1.8517500000000002     
               a) ionization =    1.5617300000000001     
               b) excitation =   0.29002000000000000     
       electron capture      =   0.99385000000000012     

