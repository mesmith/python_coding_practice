  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    33.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.897628556853505        34.257123637673807       nm 
  Mean penetration     =    12.400851656442118        9.2325696368002568       nm 
  Mean radial distance =    9.0960731292385812        8.0727923313491559       nm 
  Mean axial distance  =    6.9212869848323599        6.5733346164251314       nm 

  Final event probability : 
       energy thermalization =    3.9900000000000005E-003
       electronic excitation =    2.3900000000000002E-003
       capture (energetic)   =   0.35605000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8098700000000001     
       vib. excitation, v2   =   0.28631000000000001     
       vib. excitation, v13  =   0.69074000000000002     
       vib. excitation, vr   =   0.19571000000000002     
       Phonon excitation, t1 =    5.8270000000000002E-002
       Phonon excitation, t2 =    1.7735000000000001     
       Phonon excitation, l1 =    1.2071400000000001     
       Phonon excitation, l2 =    3.5720100000000001     
       electronic excitation =    1.4828900000000000     
               a) ionization =    1.1845100000000002     
               b) excitation =   0.29838000000000003     
       electron capture      =   0.99362000000000006     

