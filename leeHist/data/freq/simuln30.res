  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    30.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.703324189275385        34.943481286057846       nm 
  Mean penetration     =    12.293193673956450        9.2506934311581155       nm 
  Mean radial distance =    8.9596778332060065        8.0656839078168385       nm 
  Mean axial distance  =    6.9105803075815517        6.6038424950540575       nm 

  Final event probability : 
       energy thermalization =    4.1099999999999999E-003
       electronic excitation =    3.0800000000000003E-003
       capture (energetic)   =   0.38869000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7692400000000004     
       vib. excitation, v2   =   0.28547000000000000     
       vib. excitation, v13  =   0.68965000000000010     
       vib. excitation, vr   =   0.20717000000000002     
       Phonon excitation, t1 =    4.8830000000000005E-002
       Phonon excitation, t2 =    1.7953500000000002     
       Phonon excitation, l1 =    1.1901400000000002     
       Phonon excitation, l2 =    3.6167600000000002     
       electronic excitation =    1.3426700000000000     
               a) ionization =    1.0829600000000001     
               b) excitation =   0.25971000000000000     
       electron capture      =   0.99281000000000008     

