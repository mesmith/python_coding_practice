  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    58.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.058512880154375        34.701593021375814       nm 
  Mean penetration     =    13.395408199884967        9.2598663598955948       nm 
  Mean radial distance =    9.8588253490921574        8.1496790891029729       nm 
  Mean axial distance  =    7.4623445498798251        6.7736100525702589       nm 

  Final event probability : 
       energy thermalization =    4.5200000000000006E-003
       electronic excitation =    8.5000000000000006E-004
       capture (energetic)   =   0.31059000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8943400000000001     
       vib. excitation, v2   =   0.31093000000000004     
       vib. excitation, v13  =   0.69795000000000007     
       vib. excitation, vr   =   0.19553000000000001     
       Phonon excitation, t1 =    8.3430000000000004E-002
       Phonon excitation, t2 =    1.9129200000000002     
       Phonon excitation, l1 =    1.3129400000000002     
       Phonon excitation, l2 =    3.7651700000000003     
       electronic excitation =    2.2799600000000000     
               a) ionization =    1.9799400000000003     
               b) excitation =   0.30002000000000001     
       electron capture      =   0.99463000000000013     

