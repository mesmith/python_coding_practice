  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    74.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.972220953011906        34.834389657848327       nm 
  Mean penetration     =    13.588648533796313        9.2184993061433271       nm 
  Mean radial distance =    10.085560952520673        8.1158690140729348       nm 
  Mean axial distance  =    7.4950623774160405        6.7727593202810628       nm 

  Final event probability : 
       energy thermalization =    4.4500000000000000E-003
       electronic excitation =    4.9000000000000009E-004
       capture (energetic)   =   0.30606000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9002200000000000     
       vib. excitation, v2   =   0.32385000000000003     
       vib. excitation, v13  =   0.70621000000000000     
       vib. excitation, vr   =   0.19913000000000003     
       Phonon excitation, t1 =    7.9990000000000006E-002
       Phonon excitation, t2 =    1.9646800000000002     
       Phonon excitation, l1 =    1.3762800000000002     
       Phonon excitation, l2 =    3.8248800000000003     
       electronic excitation =    2.7046300000000003     
               a) ionization =    2.4006400000000001     
               b) excitation =   0.30399000000000004     
       electron capture      =   0.99506000000000006     

