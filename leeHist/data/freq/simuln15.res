  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    15.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    42.378402099561882        37.365899067586156       nm 
  Mean penetration     =    13.923342769516834        10.009163021509574       nm 
  Mean radial distance =    10.040325784598053        8.7387148920495061       nm 
  Mean axial distance  =    7.8983810267852856        7.3814033624335273       nm 

  Final event probability : 
       energy thermalization =    6.2700000000000004E-003
       electronic excitation =    8.7000000000000011E-003
       capture (energetic)   =   0.10287000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.6734700000000005     
       vib. excitation, v2   =   0.33994000000000002     
       vib. excitation, v13  =   0.77791000000000010     
       vib. excitation, vr   =   0.19180000000000003     
       Phonon excitation, t1 =    8.6830000000000004E-002
       Phonon excitation, t2 =    1.9938300000000002     
       Phonon excitation, l1 =    1.3204800000000001     
       Phonon excitation, l2 =    4.1494900000000001     
       electronic excitation =    1.0724200000000002     
               a) ionization =   0.55005999999999999     
               b) excitation =   0.52236000000000005     
       electron capture      =   0.98503000000000007     

