  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    63.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.486064394989405        34.807907006030298       nm 
  Mean penetration     =    13.467017797048102        9.2469668615011873       nm 
  Mean radial distance =    9.8916952467749724        8.1071798764120349       nm 
  Mean axial distance  =    7.5208373342438142        6.8360785448962806       nm 

  Final event probability : 
       energy thermalization =    4.5500000000000002E-003
       electronic excitation =    6.6000000000000010E-004
       capture (energetic)   =   0.30596000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9047400000000003     
       vib. excitation, v2   =   0.31683000000000000     
       vib. excitation, v13  =   0.70197000000000009     
       vib. excitation, vr   =   0.19937000000000002     
       Phonon excitation, t1 =    8.7980000000000003E-002
       Phonon excitation, t2 =    1.9361900000000001     
       Phonon excitation, l1 =    1.3428000000000002     
       Phonon excitation, l2 =    3.7839100000000001     
       electronic excitation =    2.4163700000000001     
               a) ionization =    2.1151500000000003     
               b) excitation =   0.30122000000000004     
       electron capture      =   0.99479000000000006     

