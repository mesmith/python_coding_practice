  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    5.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    49.330668172963520        37.333898777698337       nm 
  Mean penetration     =    15.530141842926255        10.195999348637860       nm 
  Mean radial distance =    11.470015027505251        9.0682221665769251       nm 
  Mean axial distance  =    8.5220557563245265        7.6631831552779417       nm 

  Final event probability : 
       energy thermalization =    2.0000000000000002E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.32625000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    4.0360900000000006     
       vib. excitation, v2   =   0.33180000000000004     
       vib. excitation, v13  =   0.85173000000000010     
       vib. excitation, vr   =   0.34124000000000004     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.6348000000000003     
       Phonon excitation, l1 =    1.4613700000000001     
       Phonon excitation, l2 =    5.3940600000000005     
       electronic excitation =   0.28783000000000003     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.28783000000000003     
       electron capture      =   0.99998000000000009     

