  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    87.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.625915964613462        34.531418214806358       nm 
  Mean penetration     =    13.868742093130550        9.2076479192309613       nm 
  Mean radial distance =    10.283905093809070        8.0957959990549568       nm 
  Mean axial distance  =    7.6635795883781794        6.8623405996859841       nm 

  Final event probability : 
       energy thermalization =    4.8900000000000002E-003
       electronic excitation =    2.8000000000000003E-004
       capture (energetic)   =   0.30780000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8851200000000001     
       vib. excitation, v2   =   0.32623000000000002     
       vib. excitation, v13  =   0.70540000000000003     
       vib. excitation, vr   =   0.19502000000000003     
       Phonon excitation, t1 =    7.7580000000000010E-002
       Phonon excitation, t2 =    1.9986000000000002     
       Phonon excitation, l1 =    1.4083000000000001     
       Phonon excitation, l2 =    3.8285600000000004     
       electronic excitation =    3.0249900000000003     
               a) ionization =    2.7132500000000004     
               b) excitation =   0.31174000000000002     
       electron capture      =   0.99483000000000010     

