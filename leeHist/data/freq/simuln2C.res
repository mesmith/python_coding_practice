  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    2.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    8.7122129630464826        5.7107784179822563       nm 
  Mean penetration     =    5.0469476589562685        3.4394271292191441       nm 
  Mean radial distance =    2.9084083274020607        2.6597682252363413       nm 
  Mean axial distance  =    3.4889373185697776        3.0976523409940344       nm 

  Final event probability : 
       energy thermalization =    1.0000000000000000E-004
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       reactive capture      =    6.2300000000000003E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.73346000000000011     
       vib. excitation, v2   =    5.3620000000000001E-002
       vib. excitation, v13  =   0.21658000000000002     
       vib. excitation, vr   =    2.0850000000000000E-002
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =   0.65431000000000006     
       Phonon excitation, l1 =   0.39154000000000005     
       Phonon excitation, l2 =    1.5686100000000001     
       electronic excitation =    0.0000000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =    0.0000000000000000     
       electron capture      =   0.99367000000000005     

