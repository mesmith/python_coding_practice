  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    3.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    24.213415198304698        14.086831028279779       nm 
  Mean penetration     =    10.175813003864381        6.2468338440780249       nm 
  Mean radial distance =    7.0509197753068582        5.3945426357007298       nm 
  Mean axial distance  =    6.0470726095328029        5.2140633189449277       nm 

  Final event probability : 
       energy thermalization =    5.0000000000000002E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       reactive capture      =    3.2500000000000003E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    1.7970700000000002     
       vib. excitation, v2   =   0.14829000000000001     
       vib. excitation, v13  =   0.52927000000000002     
       vib. excitation, vr   =   0.12241000000000000     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    1.5559300000000000     
       Phonon excitation, l1 =   0.89327000000000012     
       Phonon excitation, l2 =    3.4752200000000002     
       electronic excitation =    0.0000000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =    0.0000000000000000     
       electron capture      =   0.99670000000000003     

