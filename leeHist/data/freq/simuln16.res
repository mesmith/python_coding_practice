  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    16.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.035324698427232        35.397862956650869       nm 
  Mean penetration     =    12.610997649511587        9.4373578400925293       nm 
  Mean radial distance =    9.0020604653932796        8.2245739258397030       nm 
  Mean axial distance  =    7.2352702898977670        6.8608410319627833       nm 

  Final event probability : 
       energy thermalization =    5.9600000000000009E-003
       electronic excitation =    9.5800000000000000E-003
       capture (energetic)   =   0.13843000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.1764400000000004     
       vib. excitation, v2   =   0.30392000000000002     
       vib. excitation, v13  =   0.70302000000000009     
       vib. excitation, vr   =   0.17441000000000001     
       Phonon excitation, t1 =    6.1560000000000004E-002
       Phonon excitation, t2 =    1.8478100000000002     
       Phonon excitation, l1 =    1.1924700000000001     
       Phonon excitation, l2 =    3.7671100000000002     
       electronic excitation =    1.0712100000000000     
               a) ionization =   0.64715000000000000     
               b) excitation =   0.42406000000000005     
       electron capture      =   0.98446000000000011     

