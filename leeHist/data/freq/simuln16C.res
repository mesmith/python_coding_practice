  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    16.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.940603703853135        30.065846832113994       nm 
  Mean penetration     =    13.002590485719557        9.2482145236571878       nm 
  Mean radial distance =    9.3336357950009763        8.0997160018654117       nm 
  Mean axial distance  =    7.4196785479487497        6.8427366598032959       nm 

  Final event probability : 
       energy thermalization =    6.4100000000000008E-003
       electronic excitation =    4.0000000000000003E-005
       capture (energetic)   =   0.11425000000000000     
       reactive capture      =    3.8700000000000002E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.1756400000000005     
       vib. excitation, v2   =   0.29891000000000001     
       vib. excitation, v13  =   0.68899000000000010     
       vib. excitation, vr   =   0.15815000000000001     
       Phonon excitation, t1 =    7.5330000000000008E-002
       Phonon excitation, t2 =    1.7283000000000002     
       Phonon excitation, l1 =    1.1804400000000002     
       Phonon excitation, l2 =    3.6099200000000002     
       electronic excitation =    1.0081500000000001     
               a) ionization =   0.53100000000000003     
               b) excitation =   0.47715000000000002     
       electron capture      =   0.98968000000000012     

