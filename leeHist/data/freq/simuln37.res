  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    37.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.896189566073886        34.424657819700450       nm 
  Mean penetration     =    12.680265156408367        9.2532308403853598       nm 
  Mean radial distance =    9.3635800705812269        8.1194692117327030       nm 
  Mean axial distance  =    7.0166373439184095        6.6011963906360727       nm 

  Final event probability : 
       energy thermalization =    4.4000000000000003E-003
       electronic excitation =    1.6400000000000002E-003
       capture (energetic)   =   0.30431000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8930000000000002     
       vib. excitation, v2   =   0.29727000000000003     
       vib. excitation, v13  =   0.68930000000000002     
       vib. excitation, vr   =   0.19016000000000002     
       Phonon excitation, t1 =    6.8330000000000002E-002
       Phonon excitation, t2 =    1.8245100000000001     
       Phonon excitation, l1 =    1.2418600000000002     
       Phonon excitation, l2 =    3.6810600000000004     
       electronic excitation =    1.6612600000000002     
               a) ionization =    1.3472500000000001     
               b) excitation =   0.31401000000000001     
       electron capture      =   0.99396000000000007     

