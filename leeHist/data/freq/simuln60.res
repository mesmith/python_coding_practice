  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    60.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.092424220420398        34.548858278721781       nm 
  Mean penetration     =    13.417759513366248        9.2167675141940943       nm 
  Mean radial distance =    9.8618433663763376        8.1092033822250915       nm 
  Mean axial distance  =    7.4958672859741489        6.7662333186985686       nm 

  Final event probability : 
       energy thermalization =    4.5800000000000007E-003
       electronic excitation =    6.5000000000000008E-004
       capture (energetic)   =   0.30711000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8847900000000002     
       vib. excitation, v2   =   0.31034000000000000     
       vib. excitation, v13  =   0.69522000000000006     
       vib. excitation, vr   =   0.19613000000000003     
       Phonon excitation, t1 =    8.4970000000000004E-002
       Phonon excitation, t2 =    1.9181400000000002     
       Phonon excitation, l1 =    1.3201500000000002     
       Phonon excitation, l2 =    3.7525000000000004     
       electronic excitation =    2.3372000000000002     
               a) ionization =    2.0369300000000004     
               b) excitation =   0.30027000000000004     
       electron capture      =   0.99477000000000004     

