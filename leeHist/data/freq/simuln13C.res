  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    13.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    30.316610304381452        18.716580534227425       nm 
  Mean penetration     =    12.514637196121175        7.8210507505227040       nm 
  Mean radial distance =    8.8213560126478701        6.7796274064806168       nm 
  Mean axial distance  =    7.3276206850911878        6.3491168217032712       nm 

  Final event probability : 
       energy thermalization =    5.0400000000000002E-003
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.36455000000000004     
       reactive capture      =    4.4100000000000007E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.4845700000000002     
       vib. excitation, v2   =   0.24566000000000002     
       vib. excitation, v13  =   0.63105000000000000     
       vib. excitation, vr   =   0.13987000000000002     
       Phonon excitation, t1 =    4.1770000000000002E-002
       Phonon excitation, t2 =    1.1960400000000000     
       Phonon excitation, l1 =   0.96342000000000005     
       Phonon excitation, l2 =    2.6710700000000003     
       electronic excitation =   0.64280000000000004     
               a) ionization =   0.21273000000000003     
               b) excitation =   0.43007000000000001     
       electron capture      =   0.99054000000000009     

