  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    62.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.364725610553009        34.756227366831894       nm 
  Mean penetration     =    13.452195097411716        9.1973209300199326       nm 
  Mean radial distance =    9.8930734576655741        8.0574613210989536       nm 
  Mean axial distance  =    7.5037561566669373        6.8154474093870414       nm 

  Final event probability : 
       energy thermalization =    4.4800000000000005E-003
       electronic excitation =    6.4000000000000005E-004
       capture (energetic)   =   0.30615000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8965800000000002     
       vib. excitation, v2   =   0.31229000000000001     
       vib. excitation, v13  =   0.69735000000000003     
       vib. excitation, vr   =   0.19460000000000002     
       Phonon excitation, t1 =    8.6450000000000013E-002
       Phonon excitation, t2 =    1.9325900000000003     
       Phonon excitation, l1 =    1.3349200000000001     
       Phonon excitation, l2 =    3.7782900000000001     
       electronic excitation =    2.3900800000000002     
               a) ionization =    2.0875300000000001     
               b) excitation =   0.30255000000000004     
       electron capture      =   0.99488000000000010     

