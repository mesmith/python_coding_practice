  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    73.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.935546246833432        34.906895776742026       nm 
  Mean penetration     =    13.576899457041105        9.2232097306676906       nm 
  Mean radial distance =    10.066881009505019        8.1048442190293830       nm 
  Mean axial distance  =    7.4934014367456427        6.7983923420172179       nm 

  Final event probability : 
       energy thermalization =    4.2800000000000000E-003
       electronic excitation =    5.3000000000000009E-004
       capture (energetic)   =   0.30572000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9065600000000003     
       vib. excitation, v2   =   0.32082000000000005     
       vib. excitation, v13  =   0.70810000000000006     
       vib. excitation, vr   =   0.19886000000000001     
       Phonon excitation, t1 =    8.0170000000000005E-002
       Phonon excitation, t2 =    1.9718800000000001     
       Phonon excitation, l1 =    1.3826800000000001     
       Phonon excitation, l2 =    3.8242000000000003     
       electronic excitation =    2.6808400000000003     
               a) ionization =    2.3762800000000004     
               b) excitation =   0.30456000000000000     
       electron capture      =   0.99519000000000013     

