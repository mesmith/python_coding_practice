  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    19.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    30.982153651122310        31.699498677630814       nm 
  Mean penetration     =    11.103882614297007        8.6484422323449124       nm 
  Mean radial distance =    7.8576649411361972        7.5079228037233410       nm 
  Mean axial distance  =    6.4372588733361038        6.2081927484672104       nm 

  Final event probability : 
       energy thermalization =    5.4700000000000000E-003
       electronic excitation =    9.4100000000000000E-003
       capture (energetic)   =   0.18348000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.4596700000000000     
       vib. excitation, v2   =   0.24476000000000003     
       vib. excitation, v13  =   0.59499000000000002     
       vib. excitation, vr   =   0.16111000000000000     
       Phonon excitation, t1 =    4.0620000000000003E-002
       Phonon excitation, t2 =    1.6431500000000001     
       Phonon excitation, l1 =    1.0436800000000002     
       Phonon excitation, l2 =    3.3502700000000001     
       electronic excitation =    1.0771800000000000     
               a) ionization =   0.83999000000000001     
               b) excitation =   0.23719000000000001     
       electron capture      =   0.98512000000000011     

