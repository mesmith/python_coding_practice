  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    67.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.713851997495347        34.545933613932782       nm 
  Mean penetration     =    13.578937029899697        9.1684290802335173       nm 
  Mean radial distance =    9.9765307300912092        8.0387105915030990       nm 
  Mean axial distance  =    7.5928194356856284        6.8296912299113579       nm 

  Final event probability : 
       energy thermalization =    4.4000000000000003E-003
       electronic excitation =    5.9000000000000003E-004
       capture (energetic)   =   0.30517000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8815100000000000     
       vib. excitation, v2   =   0.31641000000000002     
       vib. excitation, v13  =   0.70088000000000006     
       vib. excitation, vr   =   0.19488000000000003     
       Phonon excitation, t1 =    8.7020000000000014E-002
       Phonon excitation, t2 =    1.9512700000000001     
       Phonon excitation, l1 =    1.3517900000000000     
       Phonon excitation, l2 =    3.7861400000000005     
       electronic excitation =    2.5287200000000003     
               a) ionization =    2.2260900000000001     
               b) excitation =   0.30263000000000001     
       electron capture      =   0.99501000000000006     

