  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    98.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.537777312736047        34.963344339082852       nm 
  Mean penetration     =    14.041236521305846        9.1944200873705224       nm 
  Mean radial distance =    10.367589799162289        8.0993571514586176       nm 
  Mean axial distance  =    7.8099681138193207        6.9001142991141071       nm 

  Final event probability : 
       energy thermalization =    4.8500000000000001E-003
       electronic excitation =    4.0000000000000002E-004
       capture (energetic)   =   0.30877000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9114300000000002     
       vib. excitation, v2   =   0.33304000000000000     
       vib. excitation, v13  =   0.71017000000000008     
       vib. excitation, vr   =   0.19692000000000001     
       Phonon excitation, t1 =    8.0060000000000006E-002
       Phonon excitation, t2 =    2.0252400000000002     
       Phonon excitation, l1 =    1.4433700000000000     
       Phonon excitation, l2 =    3.8920800000000004     
       electronic excitation =    3.2876400000000001     
               a) ionization =    2.9716600000000004     
               b) excitation =   0.31598000000000004     
       electron capture      =   0.99475000000000013     

