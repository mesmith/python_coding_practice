  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    53.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.594745435179703        34.784175322983138       nm 
  Mean penetration     =    13.227835641181006        9.2412496055961171       nm 
  Mean radial distance =    9.7032946881446041        8.0650174637958809       nm 
  Mean axial distance  =    7.4049376661153357        6.8077010574544037       nm 

  Final event probability : 
       energy thermalization =    4.1700000000000001E-003
       electronic excitation =    7.3000000000000007E-004
       capture (energetic)   =   0.31075000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8853900000000001     
       vib. excitation, v2   =   0.30706000000000000     
       vib. excitation, v13  =   0.69741000000000009     
       vib. excitation, vr   =   0.19266000000000003     
       Phonon excitation, t1 =    7.8220000000000012E-002
       Phonon excitation, t2 =    1.8980900000000001     
       Phonon excitation, l1 =    1.2935900000000000     
       Phonon excitation, l2 =    3.7446000000000002     
       electronic excitation =    2.1295400000000000     
               a) ionization =    1.8383900000000002     
               b) excitation =   0.29115000000000002     
       electron capture      =   0.99510000000000010     

