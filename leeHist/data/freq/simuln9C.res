  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    9.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    25.292784678527497        33.719966093394575       nm 
  Mean penetration     =    8.8773868958591766        8.4341864767385726       nm 
  Mean radial distance =    5.9321570824794119        7.2873564119256171       nm 
  Mean axial distance  =    5.3608421632984662        5.7366209389961496       nm 

  Final event probability : 
       energy thermalization =    7.9000000000000008E-003
       electronic excitation =    6.2000000000000000E-004
       capture (energetic)   =   0.70417000000000007     
       reactive capture      =    6.4500000000000009E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.1548700000000003     
       vib. excitation, v2   =   0.25469000000000003     
       vib. excitation, v13  =   0.75721000000000005     
       vib. excitation, vr   =   0.26220000000000004     
       Phonon excitation, t1 =    1.3090000000000001E-002
       Phonon excitation, t2 =    1.2787800000000000     
       Phonon excitation, l1 =   0.85046000000000010     
       Phonon excitation, l2 =    2.7651000000000003     
       electronic excitation =   0.21366000000000002     
               a) ionization =    6.9600000000000009E-003
               b) excitation =   0.20670000000000002     
       electron capture      =   0.98501000000000005     

