  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    66.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.774415294710344        34.695012560973339       nm 
  Mean penetration     =    13.580206162237729        9.1972615514109162       nm 
  Mean radial distance =    9.9513187120242819        8.0463522947746782       nm 
  Mean axial distance  =    7.6145492828073165        6.8744257442881977       nm 

  Final event probability : 
       energy thermalization =    4.5900000000000003E-003
       electronic excitation =    5.9000000000000003E-004
       capture (energetic)   =   0.30180000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8960100000000004     
       vib. excitation, v2   =   0.31795000000000001     
       vib. excitation, v13  =   0.70535000000000003     
       vib. excitation, vr   =   0.19639000000000001     
       Phonon excitation, t1 =    8.7790000000000007E-002
       Phonon excitation, t2 =    1.9550800000000002     
       Phonon excitation, l1 =    1.3552400000000000     
       Phonon excitation, l2 =    3.8033300000000003     
       electronic excitation =    2.5012100000000004     
               a) ionization =    2.1971200000000000     
               b) excitation =   0.30409000000000003     
       electron capture      =   0.99482000000000004     

