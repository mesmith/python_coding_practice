  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    52.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.637828899582225        34.907003075334814       nm 
  Mean penetration     =    13.244567015498317        9.2938847655466788       nm 
  Mean radial distance =    9.7015675430005341        8.1085827672769319       nm 
  Mean axial distance  =    7.4380416983021442        6.8264821007909005       nm 

  Final event probability : 
       energy thermalization =    4.0200000000000001E-003
       electronic excitation =    8.8000000000000003E-004
       capture (energetic)   =   0.31135000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8983700000000003     
       vib. excitation, v2   =   0.30725000000000002     
       vib. excitation, v13  =   0.70430000000000004     
       vib. excitation, vr   =   0.19406000000000001     
       Phonon excitation, t1 =    7.5560000000000002E-002
       Phonon excitation, t2 =    1.9045900000000002     
       Phonon excitation, l1 =    1.2911600000000001     
       Phonon excitation, l2 =    3.7631500000000004     
       electronic excitation =    2.1002700000000001     
               a) ionization =    1.8098500000000002     
               b) excitation =   0.29042000000000001     
       electron capture      =   0.99510000000000010     

