  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    14.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    45.108314447070597        36.558344144522096       nm 
  Mean penetration     =    14.732361180913344        10.144905370448384       nm 
  Mean radial distance =    10.686578351823407        8.9042059895928034       nm 
  Mean axial distance  =    8.3038110686613180        7.5842238562325788       nm 

  Final event probability : 
       energy thermalization =    5.4800000000000005E-003
       electronic excitation =    7.0800000000000004E-003
       capture (energetic)   =    7.1460000000000010E-002
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.8952800000000005     
       vib. excitation, v2   =   0.36303000000000002     
       vib. excitation, v13  =   0.83426000000000011     
       vib. excitation, vr   =   0.20113000000000000     
       Phonon excitation, t1 =   0.10414000000000001     
       Phonon excitation, t2 =    2.0339800000000001     
       Phonon excitation, l1 =    1.4060400000000002     
       Phonon excitation, l2 =    4.2922000000000002     
       electronic excitation =    1.0524300000000000     
               a) ionization =   0.45001000000000002     
               b) excitation =   0.60242000000000007     
       electron capture      =   0.98744000000000010     

