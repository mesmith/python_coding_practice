  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    9.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    17.763816858797160        25.473535589736223       nm 
  Mean penetration     =    7.3688843672235844        6.6652786582050734       nm 
  Mean radial distance =    4.6682151874388476        5.7485431088856238       nm 
  Mean axial distance  =    4.6518885934256833        4.7168154524466637       nm 

  Final event probability : 
       energy thermalization =    8.2000000000000007E-003
       electronic excitation =    9.2500000000000013E-003
       capture (energetic)   =   0.73092000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    1.4074700000000002     
       vib. excitation, v2   =   0.19259000000000001     
       vib. excitation, v13  =   0.60353000000000001     
       vib. excitation, vr   =   0.20701000000000003     
       Phonon excitation, t1 =    1.8600000000000002E-002
       Phonon excitation, t2 =   0.79363000000000006     
       Phonon excitation, l1 =   0.63471000000000000     
       Phonon excitation, l2 =    1.8532400000000002     
       electronic excitation =   0.26008000000000003     
               a) ionization =    1.4550000000000000E-002
               b) excitation =   0.24553000000000003     
       electron capture      =   0.98255000000000003     

