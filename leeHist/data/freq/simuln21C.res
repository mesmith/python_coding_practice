  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    21.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    29.634209234675389        27.930270050023761       nm 
  Mean penetration     =    11.038557034437543        8.3314556489426579       nm 
  Mean radial distance =    7.8309132382407043        7.2568518640750597       nm 
  Mean axial distance  =    6.3968713292081496        6.0297454928380585       nm 

  Final event probability : 
       energy thermalization =    5.7800000000000004E-003
       electronic excitation =    9.0000000000000006E-005
       capture (energetic)   =   0.16240000000000002     
       reactive capture      =    3.1600000000000000E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.2851200000000000     
       vib. excitation, v2   =   0.22510000000000002     
       vib. excitation, v13  =   0.55284000000000000     
       vib. excitation, vr   =   0.14521000000000001     
       Phonon excitation, t1 =    3.7800000000000000E-002
       Phonon excitation, t2 =    1.5774800000000002     
       Phonon excitation, l1 =    1.0079000000000000     
       Phonon excitation, l2 =    3.2161600000000004     
       electronic excitation =    1.0734800000000000     
               a) ionization =   0.87523000000000006     
               b) excitation =   0.19825000000000001     
       electron capture      =   0.99094000000000004     

