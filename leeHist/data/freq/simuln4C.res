  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    4.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.454044839168681        22.723841195688564       nm 
  Mean penetration     =    13.553050673508960        8.2979909335332973       nm 
  Mean radial distance =    9.7680690045740093        7.3479066273723515       nm 
  Mean axial distance  =    7.6914108570112916        6.6315255588475832       nm 

  Final event probability : 
       energy thermalization =    3.0000000000000004E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       reactive capture      =    2.9800000000000004E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7391000000000001     
       vib. excitation, v2   =   0.23261000000000001     
       vib. excitation, v13  =   0.70273000000000008     
       vib. excitation, vr   =   0.23649000000000001     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.1617400000000000     
       Phonon excitation, l1 =    1.2085700000000001     
       Phonon excitation, l2 =    4.5654000000000003     
       electronic excitation =    6.5140000000000003E-002
               a) ionization =    0.0000000000000000     
               b) excitation =    6.5140000000000003E-002
       electron capture      =   0.99699000000000004     

