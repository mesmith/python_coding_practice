  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    23.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    31.017714725735530        28.549245938724773       nm 
  Mean penetration     =    11.388963697686124        8.5099331305059280       nm 
  Mean radial distance =    8.1463830625958451        7.3952686235675955       nm 
  Mean axial distance  =    6.5418206449426615        6.1869607744856889       nm 

  Final event probability : 
       energy thermalization =    4.7100000000000006E-003
       electronic excitation =    7.0000000000000007E-005
       capture (energetic)   =   0.18542000000000000     
       reactive capture      =    2.0500000000000002E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.3466900000000002     
       vib. excitation, v2   =   0.23325000000000001     
       vib. excitation, v13  =   0.57276000000000005     
       vib. excitation, vr   =   0.16481000000000001     
       Phonon excitation, t1 =    3.5520000000000003E-002
       Phonon excitation, t2 =    1.6472400000000000     
       Phonon excitation, l1 =    1.0454500000000000     
       Phonon excitation, l2 =    3.3733600000000004     
       electronic excitation =    1.1013500000000001     
               a) ionization =   0.92598000000000003     
               b) excitation =   0.17537000000000003     
       electron capture      =   0.99316000000000004     

