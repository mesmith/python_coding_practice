  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    94.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.355804415998819        34.990065891129866       nm 
  Mean penetration     =    14.022926093814181        9.2566233536654376       nm 
  Mean radial distance =    10.349976201345022        8.1573795455604881       nm 
  Mean axial distance  =    7.7938105589306117        6.9223695868513024       nm 

  Final event probability : 
       energy thermalization =    4.4600000000000004E-003
       electronic excitation =    3.8000000000000002E-004
       capture (energetic)   =   0.30690000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9059500000000003     
       vib. excitation, v2   =   0.33070000000000005     
       vib. excitation, v13  =   0.71546000000000010     
       vib. excitation, vr   =   0.19566000000000003     
       Phonon excitation, t1 =    8.0480000000000010E-002
       Phonon excitation, t2 =    2.0158700000000001     
       Phonon excitation, l1 =    1.4328700000000001     
       Phonon excitation, l2 =    3.8674900000000001     
       electronic excitation =    3.1944800000000004     
               a) ionization =    2.8815100000000000     
               b) excitation =   0.31297000000000003     
       electron capture      =   0.99516000000000004     

