  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    59.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.923166087601693        34.510909743169421       nm 
  Mean penetration     =    13.372198493582401        9.2152256620969606       nm 
  Mean radial distance =    9.8276651047045078        8.1256388707698157       nm 
  Mean axial distance  =    7.4746093942264791        6.7273533050144723       nm 

  Final event probability : 
       energy thermalization =    4.7700000000000008E-003
       electronic excitation =    6.9000000000000008E-004
       capture (energetic)   =   0.30975000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8781600000000003     
       vib. excitation, v2   =   0.31009000000000003     
       vib. excitation, v13  =   0.69237000000000004     
       vib. excitation, vr   =   0.19382000000000002     
       Phonon excitation, t1 =    8.3920000000000008E-002
       Phonon excitation, t2 =    1.9126700000000001     
       Phonon excitation, l1 =    1.3079800000000001     
       Phonon excitation, l2 =    3.7504200000000001     
       electronic excitation =    2.3047200000000001     
               a) ionization =    2.0056400000000001     
               b) excitation =   0.29908000000000001     
       electron capture      =   0.99454000000000009     

