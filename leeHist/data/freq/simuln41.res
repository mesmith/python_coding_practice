  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    41.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.185602722234712        34.269944062743917       nm 
  Mean penetration     =    12.765824822030321        9.1552196594596396       nm 
  Mean radial distance =    9.4023305270807480        8.0133765656805362       nm 
  Mean axial distance  =    7.1011221474046593        6.6136503780809424       nm 

  Final event probability : 
       energy thermalization =    4.6600000000000001E-003
       electronic excitation =    1.1000000000000001E-003
       capture (energetic)   =   0.28483000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8709600000000002     
       vib. excitation, v2   =   0.29753000000000002     
       vib. excitation, v13  =   0.67678000000000005     
       vib. excitation, vr   =   0.18822000000000003     
       Phonon excitation, t1 =    7.2989999999999999E-002
       Phonon excitation, t2 =    1.8564900000000002     
       Phonon excitation, l1 =    1.2468300000000001     
       Phonon excitation, l2 =    3.7124100000000002     
       electronic excitation =    1.7960600000000002     
               a) ionization =    1.4986800000000000     
               b) excitation =   0.29738000000000003     
       electron capture      =   0.99424000000000012     

