  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    4.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    45.580953972262527        31.664269508982496       nm 
  Mean penetration     =    14.999974963505956        9.6333612867648473       nm 
  Mean radial distance =    10.923935064568010        8.5434404521342859       nm 
  Mean axial distance  =    8.3997586310818679        7.4109528046432906       nm 

  Final event probability : 
       energy thermalization =    2.0000000000000002E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.3842300000000001     
       vib. excitation, v2   =   0.28198000000000001     
       vib. excitation, v13  =   0.80958000000000008     
       vib. excitation, vr   =   0.30481000000000003     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.5371800000000002     
       Phonon excitation, l1 =    1.4158700000000002     
       Phonon excitation, l2 =    5.2658700000000005     
       electronic excitation =   0.13383000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.13383000000000000     
       electron capture      =   0.99998000000000009     

