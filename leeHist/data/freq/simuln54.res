  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    54.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.567463893923872        34.582292934983819       nm 
  Mean penetration     =    13.249213198087697        9.2172737584011415       nm 
  Mean radial distance =    9.7489457648972309        8.0760935561043503       nm 
  Mean axial distance  =    7.3948676648623843        6.7491101243796452       nm 

  Final event probability : 
       energy thermalization =    4.5000000000000005E-003
       electronic excitation =    8.2000000000000009E-004
       capture (energetic)   =   0.31371000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8785500000000002     
       vib. excitation, v2   =   0.30815000000000003     
       vib. excitation, v13  =   0.69791000000000003     
       vib. excitation, vr   =   0.19363000000000002     
       Phonon excitation, t1 =    7.9420000000000004E-002
       Phonon excitation, t2 =    1.9014100000000000     
       Phonon excitation, l1 =    1.2993300000000001     
       Phonon excitation, l2 =    3.7357000000000005     
       electronic excitation =    2.1599800000000000     
               a) ionization =    1.8673900000000001     
               b) excitation =   0.29259000000000002     
       electron capture      =   0.99468000000000012     

