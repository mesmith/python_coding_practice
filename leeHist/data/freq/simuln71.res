  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    71.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.718567393477848        34.887305276169720       nm 
  Mean penetration     =    13.529669468137742        9.2152637376890354       nm 
  Mean radial distance =    10.002760503589014        8.0785083624373826       nm 
  Mean axial distance  =    7.5010443894621552        6.8110101809798413       nm 

  Final event probability : 
       energy thermalization =    4.4700000000000000E-003
       electronic excitation =    6.1000000000000008E-004
       capture (energetic)   =   0.30472000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8998800000000005     
       vib. excitation, v2   =   0.31744000000000000     
       vib. excitation, v13  =   0.70348000000000011     
       vib. excitation, vr   =   0.19861000000000001     
       Phonon excitation, t1 =    8.0150000000000013E-002
       Phonon excitation, t2 =    1.9613600000000002     
       Phonon excitation, l1 =    1.3671700000000002     
       Phonon excitation, l2 =    3.8158700000000003     
       electronic excitation =    2.6358300000000003     
               a) ionization =    2.3308800000000001     
               b) excitation =   0.30495000000000000     
       electron capture      =   0.99492000000000003     

