  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    100.00000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.684659020593998        34.718644457901227       nm 
  Mean penetration     =    14.096336122606060        9.1680877921667800       nm 
  Mean radial distance =    10.403186669607924        8.0729820976317903       nm 
  Mean axial distance  =    7.8452212898641305        6.9147448201678827       nm 

  Final event probability : 
       energy thermalization =    4.6500000000000005E-003
       electronic excitation =    4.6000000000000001E-004
       capture (energetic)   =   0.30761000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9165000000000001     
       vib. excitation, v2   =   0.33602000000000004     
       vib. excitation, v13  =   0.71628000000000003     
       vib. excitation, vr   =   0.19690000000000002     
       Phonon excitation, t1 =    7.9760000000000011E-002
       Phonon excitation, t2 =    2.0292300000000001     
       Phonon excitation, l1 =    1.4519300000000002     
       Phonon excitation, l2 =    3.8890900000000004     
       electronic excitation =    3.3281600000000005     
               a) ionization =    3.0133100000000002     
               b) excitation =   0.31485000000000002     
       electron capture      =   0.99489000000000005     

