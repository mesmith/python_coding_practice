  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    93.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.258722424849395        34.870713074504458       nm 
  Mean penetration     =    14.032380909234480        9.2662462482618189       nm 
  Mean radial distance =    10.361460662871577        8.1512569450343406       nm 
  Mean axial distance  =    7.7958027648983697        6.9421636334697947       nm 

  Final event probability : 
       energy thermalization =    4.1900000000000001E-003
       electronic excitation =    3.7000000000000005E-004
       capture (energetic)   =   0.30501000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8998700000000004     
       vib. excitation, v2   =   0.33158000000000004     
       vib. excitation, v13  =   0.71010000000000006     
       vib. excitation, vr   =   0.19347000000000000     
       Phonon excitation, t1 =    7.9580000000000012E-002
       Phonon excitation, t2 =    2.0166400000000002     
       Phonon excitation, l1 =    1.4267900000000002     
       Phonon excitation, l2 =    3.8522800000000004     
       electronic excitation =    3.1708600000000002     
               a) ionization =    2.8593100000000002     
               b) excitation =   0.31155000000000005     
       electron capture      =   0.99544000000000010     

