  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    8.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    34.070093731413316        38.349574671331091       nm 
  Mean penetration     =    10.754503539563148        9.6658999513770727       nm 
  Mean radial distance =    7.5590624765579015        8.4569345696988485       nm 
  Mean axial distance  =    6.1707861046642147        6.5077799027254848       nm 

  Final event probability : 
       energy thermalization =    3.4500000000000004E-003
       electronic excitation =    1.1000000000000001E-003
       capture (energetic)   =   0.70274000000000003     
       reactive capture      =    5.7300000000000007E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.0600300000000002     
       vib. excitation, v2   =   0.31850000000000001     
       vib. excitation, v13  =   0.90042000000000011     
       vib. excitation, vr   =   0.31137000000000004     
       Phonon excitation, t1 =    2.2800000000000003E-003
       Phonon excitation, t2 =    1.8714100000000002     
       Phonon excitation, l1 =    1.0859100000000002     
       Phonon excitation, l2 =    3.8259100000000004     
       electronic excitation =   0.13445000000000001     
               a) ionization =    5.5000000000000003E-004
               b) excitation =   0.13390000000000002     
       electron capture      =   0.98972000000000004     

