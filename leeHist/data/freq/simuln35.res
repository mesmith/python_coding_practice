  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    35.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    36.525245046944008        34.366349076320752       nm 
  Mean penetration     =    12.577806298138672        9.2662545462658557       nm 
  Mean radial distance =    9.2749504867245527        8.1157472712864536       nm 
  Mean axial distance  =    6.9831383361157533        6.5886571671145564       nm 

  Final event probability : 
       energy thermalization =    4.1099999999999999E-003
       electronic excitation =    1.9700000000000000E-003
       capture (energetic)   =   0.32439000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8608900000000004     
       vib. excitation, v2   =   0.29450000000000004     
       vib. excitation, v13  =   0.69205000000000005     
       vib. excitation, vr   =   0.19368000000000002     
       Phonon excitation, t1 =    6.4399999999999999E-002
       Phonon excitation, t2 =    1.8022400000000001     
       Phonon excitation, l1 =    1.2250900000000000     
       Phonon excitation, l2 =    3.6216800000000005     
       electronic excitation =    1.5801600000000002     
               a) ionization =    1.2654300000000001     
               b) excitation =   0.31473000000000001     
       electron capture      =   0.99392000000000014     

