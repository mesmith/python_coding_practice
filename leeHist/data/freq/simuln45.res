  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    45.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.592003096911903        34.396954608202350       nm 
  Mean penetration     =    12.907643779991339        9.2126109930491999       nm 
  Mean radial distance =    9.4956306797067001        8.0683168711064894       nm 
  Mean axial distance  =    7.1926941724827005        6.6693238535524966       nm 

  Final event probability : 
       energy thermalization =    4.4800000000000005E-003
       electronic excitation =    1.2500000000000000E-003
       capture (energetic)   =   0.29224000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8630700000000004     
       vib. excitation, v2   =   0.29633000000000004     
       vib. excitation, v13  =   0.68413000000000002     
       vib. excitation, vr   =   0.19039000000000000     
       Phonon excitation, t1 =    7.3680000000000009E-002
       Phonon excitation, t2 =    1.8766400000000001     
       Phonon excitation, l1 =    1.2599200000000002     
       Phonon excitation, l2 =    3.7311900000000002     
       electronic excitation =    1.9070300000000002     
               a) ionization =    1.6245000000000001     
               b) excitation =   0.28253000000000000     
       electron capture      =   0.99427000000000010     

