  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    42.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.336786765892938        34.503716587665025       nm 
  Mean penetration     =    12.792125354660264        9.1862044064016999       nm 
  Mean radial distance =    9.4308615976737951        8.0638456619813095       nm 
  Mean axial distance  =    7.1010663171581045        6.6055221073072250       nm 

  Final event probability : 
       energy thermalization =    4.6700000000000005E-003
       electronic excitation =    1.2600000000000001E-003
       capture (energetic)   =   0.28436000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8724800000000004     
       vib. excitation, v2   =   0.29829000000000000     
       vib. excitation, v13  =   0.67784000000000011     
       vib. excitation, vr   =   0.18962000000000001     
       Phonon excitation, t1 =    7.3790000000000008E-002
       Phonon excitation, t2 =    1.8584400000000001     
       Phonon excitation, l1 =    1.2538700000000000     
       Phonon excitation, l2 =    3.7083100000000004     
       electronic excitation =    1.8228500000000001     
               a) ionization =    1.5289800000000002     
               b) excitation =   0.29387000000000002     
       electron capture      =   0.99407000000000012     

