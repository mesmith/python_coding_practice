  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    2.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    13.137206282836127        8.6429699964354540       nm 
  Mean penetration     =    6.5340579896073301        4.3625320141786030       nm 
  Mean radial distance =    4.2578570314887205        3.6777006028157340       nm 
  Mean axial distance  =    4.1011573039207301        3.6402307523333177       nm 

  Final event probability : 
       energy thermalization =    5.0000000000000002E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    1.0922200000000002     
       vib. excitation, v2   =    8.3980000000000013E-002
       vib. excitation, v13  =   0.32629000000000002     
       vib. excitation, vr   =    4.1770000000000002E-002
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =   0.97737000000000007     
       Phonon excitation, l1 =   0.57085000000000008     
       Phonon excitation, l2 =    2.2918400000000001     
       electronic excitation =    0.0000000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =    0.0000000000000000     
       electron capture      =   0.99995000000000012     

