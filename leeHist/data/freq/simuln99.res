  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    99.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.655913052847083        34.828969324539145       nm 
  Mean penetration     =    14.130322625380435        9.2580459218726698       nm 
  Mean radial distance =    10.433467864900372        8.1492006896710514       nm 
  Mean axial distance  =    7.8551615836784201        6.9575243236871609       nm 

  Final event probability : 
       energy thermalization =    4.5900000000000003E-003
       electronic excitation =    3.3000000000000005E-004
       capture (energetic)   =   0.30683000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9113800000000003     
       vib. excitation, v2   =   0.33372000000000002     
       vib. excitation, v13  =   0.71175000000000010     
       vib. excitation, vr   =   0.19420000000000001     
       Phonon excitation, t1 =    7.9430000000000001E-002
       Phonon excitation, t2 =    2.0290700000000004     
       Phonon excitation, l1 =    1.4525000000000001     
       Phonon excitation, l2 =    3.8849500000000003     
       electronic excitation =    3.3138000000000001     
               a) ionization =    2.9978700000000003     
               b) excitation =   0.31593000000000004     
       electron capture      =   0.99508000000000008     

