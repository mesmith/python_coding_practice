  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    49.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.357209715987345        34.658662269912490       nm 
  Mean penetration     =    13.127481369363499        9.1579733223709621       nm 
  Mean radial distance =    9.6463762999501110        8.0392823526484545       nm 
  Mean axial distance  =    7.3328837696646412        6.6892018680101151       nm 

  Final event probability : 
       energy thermalization =    4.0600000000000002E-003
       electronic excitation =    1.0300000000000001E-003
       capture (energetic)   =   0.30858000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8815200000000001     
       vib. excitation, v2   =   0.30188000000000004     
       vib. excitation, v13  =   0.68965000000000010     
       vib. excitation, vr   =   0.19939000000000001     
       Phonon excitation, t1 =    7.5020000000000003E-002
       Phonon excitation, t2 =    1.9012800000000001     
       Phonon excitation, l1 =    1.2868300000000001     
       Phonon excitation, l2 =    3.7517600000000004     
       electronic excitation =    2.0179100000000001     
               a) ionization =    1.7307000000000001     
               b) excitation =   0.28721000000000002     
       electron capture      =   0.99491000000000007     

