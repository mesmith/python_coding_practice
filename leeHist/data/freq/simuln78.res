  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    78.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.777204461702752        34.718964889319466       nm 
  Mean penetration     =    13.521736048107991        9.1271416804222252       nm 
  Mean radial distance =    10.056213725612555        8.0301342118051071       nm 
  Mean axial distance  =    7.4375893180752399        6.7241234514289081       nm 

  Final event probability : 
       energy thermalization =    4.6800000000000001E-003
       electronic excitation =    5.2000000000000006E-004
       capture (energetic)   =   0.30932000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8864800000000002     
       vib. excitation, v2   =   0.32172000000000001     
       vib. excitation, v13  =   0.70316000000000001     
       vib. excitation, vr   =   0.19311000000000000     
       Phonon excitation, t1 =    7.7240000000000003E-002
       Phonon excitation, t2 =    1.9600600000000001     
       Phonon excitation, l1 =    1.3839800000000002     
       Phonon excitation, l2 =    3.8110700000000004     
       electronic excitation =    2.8042000000000002     
               a) ionization =    2.5008200000000000     
               b) excitation =   0.30338000000000004     
       electron capture      =   0.99480000000000013     

