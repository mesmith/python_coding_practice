  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    24.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    32.075192998925765        29.030538611801351       nm 
  Mean penetration     =    11.651478302824971        8.6259359079345543       nm 
  Mean radial distance =    8.3950066961448737        7.5025418355779854       nm 
  Mean axial distance  =    6.6437962319793789        6.2657335671106802       nm 

  Final event probability : 
       energy thermalization =    4.2400000000000007E-003
       electronic excitation =    1.7000000000000001E-004
       capture (energetic)   =   0.20690000000000003     
       reactive capture      =    1.9800000000000000E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.4167200000000002     
       vib. excitation, v2   =   0.24115000000000003     
       vib. excitation, v13  =   0.59233000000000002     
       vib. excitation, vr   =   0.17266000000000001     
       Phonon excitation, t1 =    3.5150000000000001E-002
       Phonon excitation, t2 =    1.7026000000000001     
       Phonon excitation, l1 =    1.0807200000000001     
       Phonon excitation, l2 =    3.4712900000000002     
       electronic excitation =    1.1198100000000002     
               a) ionization =   0.94165000000000010     
               b) excitation =   0.17816000000000001     
       electron capture      =   0.99360000000000004     

