  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    31.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.716948471000535        34.860393907230147       nm 
  Mean penetration     =    12.280587605039660        9.2175380007418006       nm 
  Mean radial distance =    8.9619174786931080        8.0237830101906340       nm 
  Mean axial distance  =    6.9067563020646592        6.5860076992465757       nm 

  Final event probability : 
       energy thermalization =    3.6700000000000001E-003
       electronic excitation =    2.7200000000000002E-003
       capture (energetic)   =   0.38556000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7865700000000002     
       vib. excitation, v2   =   0.28799000000000002     
       vib. excitation, v13  =   0.68794000000000011     
       vib. excitation, vr   =   0.20240000000000002     
       Phonon excitation, t1 =    5.2470000000000003E-002
       Phonon excitation, t2 =    1.7710800000000002     
       Phonon excitation, l1 =    1.1903200000000000     
       Phonon excitation, l2 =    3.5824500000000001     
       electronic excitation =    1.3855200000000001     
               a) ionization =    1.1105800000000001     
               b) excitation =   0.27494000000000002     
       electron capture      =   0.99361000000000010     

