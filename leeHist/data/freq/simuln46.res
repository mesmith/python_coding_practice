  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    46.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.687036287286091        34.553102149445415       nm 
  Mean penetration     =    12.955283221107820        9.2004017808005898       nm 
  Mean radial distance =    9.5275828696853342        8.0539676538362794       nm 
  Mean axial distance  =    7.2312025245169682        6.6749708620291166       nm 

  Final event probability : 
       energy thermalization =    4.4300000000000008E-003
       electronic excitation =    1.1500000000000002E-003
       capture (energetic)   =   0.29280000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8638800000000004     
       vib. excitation, v2   =   0.29820000000000002     
       vib. excitation, v13  =   0.68515000000000004     
       vib. excitation, vr   =   0.19345000000000001     
       Phonon excitation, t1 =    7.5410000000000005E-002
       Phonon excitation, t2 =    1.8754900000000001     
       Phonon excitation, l1 =    1.2630900000000000     
       Phonon excitation, l2 =    3.7274500000000002     
       electronic excitation =    1.9320800000000002     
               a) ionization =    1.6506800000000001     
               b) excitation =   0.28140000000000004     
       electron capture      =   0.99442000000000008     

