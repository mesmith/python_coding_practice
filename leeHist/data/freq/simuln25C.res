  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    25.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    33.050733428528872        29.698512353434388       nm 
  Mean penetration     =    11.837390379245644        8.7049250741638673       nm 
  Mean radial distance =    8.5392285996831365        7.6008680250293752       nm 
  Mean axial distance  =    6.7327414310349960        6.3149112858956977       nm 

  Final event probability : 
       energy thermalization =    4.0200000000000001E-003
       electronic excitation =    9.0000000000000006E-005
       capture (energetic)   =   0.23240000000000002     
       reactive capture      =    1.6700000000000000E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.4934400000000001     
       vib. excitation, v2   =   0.24727000000000002     
       vib. excitation, v13  =   0.61269000000000007     
       vib. excitation, vr   =   0.17980000000000002     
       Phonon excitation, t1 =    3.4960000000000005E-002
       Phonon excitation, t2 =    1.7557700000000001     
       Phonon excitation, l1 =    1.1061700000000001     
       Phonon excitation, l2 =    3.5664600000000002     
       electronic excitation =    1.1392800000000001     
               a) ionization =   0.95759000000000005     
               b) excitation =   0.18169000000000002     
       electron capture      =   0.99421000000000004     

