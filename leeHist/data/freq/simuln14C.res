  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    14.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.950466587600268        23.646793127174096       nm 
  Mean penetration     =    13.501390581873356        8.6906252625128939       nm 
  Mean radial distance =    9.6772207648238986        7.6200454100158463       nm 
  Mean axial distance  =    7.7396184097882577        6.7969940552238421       nm 

  Final event probability : 
       energy thermalization =    6.2300000000000003E-003
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.21589000000000003     
       reactive capture      =    4.0700000000000007E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.0430000000000001     
       vib. excitation, v2   =   0.29441000000000001     
       vib. excitation, v13  =   0.70610000000000006     
       vib. excitation, vr   =   0.15695000000000001     
       Phonon excitation, t1 =    7.3080000000000006E-002
       Phonon excitation, t2 =    1.5066200000000001     
       Phonon excitation, l1 =    1.1334700000000002     
       Phonon excitation, l2 =    3.2840500000000001     
       electronic excitation =   0.82256000000000007     
               a) ionization =   0.31709000000000004     
               b) excitation =   0.50547000000000009     
       electron capture      =   0.98969000000000007     

