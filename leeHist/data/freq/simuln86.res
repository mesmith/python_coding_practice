  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    86.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.631963918188724        34.751972669334577       nm 
  Mean penetration     =    13.814254813842624        9.1865869477933053       nm 
  Mean radial distance =    10.222944445106597        8.0997470927668900       nm 
  Mean axial distance  =    7.6497462792454574        6.8259725739114510       nm 

  Final event probability : 
       energy thermalization =    5.0000000000000001E-003
       electronic excitation =    3.4000000000000002E-004
       capture (energetic)   =   0.30745000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8887900000000002     
       vib. excitation, v2   =   0.32665000000000005     
       vib. excitation, v13  =   0.70851000000000008     
       vib. excitation, vr   =   0.19555000000000003     
       Phonon excitation, t1 =    7.7060000000000003E-002
       Phonon excitation, t2 =    1.9947100000000002     
       Phonon excitation, l1 =    1.4054700000000002     
       Phonon excitation, l2 =    3.8347300000000004     
       electronic excitation =    3.0005700000000002     
               a) ionization =    2.6915700000000000     
               b) excitation =   0.30900000000000005     
       electron capture      =   0.99466000000000010     

