  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    65.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.647608030265154        34.679744378134416       nm 
  Mean penetration     =    13.493649008993327        9.1628073727823498       nm 
  Mean radial distance =    9.9257477077819392        8.0597998551885723       nm 
  Mean axial distance  =    7.5331951466283833        6.7679932141810646       nm 

  Final event probability : 
       energy thermalization =    4.9300000000000004E-003
       electronic excitation =    5.0000000000000001E-004
       capture (energetic)   =   0.30416000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9005100000000001     
       vib. excitation, v2   =   0.31801000000000001     
       vib. excitation, v13  =   0.70089000000000001     
       vib. excitation, vr   =   0.19386000000000000     
       Phonon excitation, t1 =    8.6550000000000002E-002
       Phonon excitation, t2 =    1.9410900000000002     
       Phonon excitation, l1 =    1.3464100000000001     
       Phonon excitation, l2 =    3.8129200000000005     
       electronic excitation =    2.4707900000000000     
               a) ionization =    2.1691000000000003     
               b) excitation =   0.30169000000000001     
       electron capture      =   0.99457000000000007     

