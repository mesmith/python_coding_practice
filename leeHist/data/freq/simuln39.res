  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    39.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.187957791099741        34.370136679296607       nm 
  Mean penetration     =    12.737559127791039        9.1612765781518704       nm 
  Mean radial distance =    9.4089470314199346        8.0394989435928945       nm 
  Mean axial distance  =    7.0638062885097703        6.5662176547599804       nm 

  Final event probability : 
       energy thermalization =    4.7000000000000002E-003
       electronic excitation =    1.6200000000000001E-003
       capture (energetic)   =   0.28685000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8975800000000000     
       vib. excitation, v2   =   0.29824000000000001     
       vib. excitation, v13  =   0.68162000000000000     
       vib. excitation, vr   =   0.19314000000000001     
       Phonon excitation, t1 =    7.2110000000000007E-002
       Phonon excitation, t2 =    1.8545700000000001     
       Phonon excitation, l1 =    1.2483600000000001     
       Phonon excitation, l2 =    3.7046400000000004     
       electronic excitation =    1.7364000000000002     
               a) ionization =    1.4276600000000002     
               b) excitation =   0.30874000000000001     
       electron capture      =   0.99368000000000012     

