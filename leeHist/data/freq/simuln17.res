  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    17.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    33.689727892354526        33.284176732672741       nm 
  Mean penetration     =    11.729736527027496        8.9491882808803780       nm 
  Mean radial distance =    8.3484547038610497        7.7760309375099474       nm 
  Mean axial distance  =    6.7546015943591211        6.4719930545452584       nm 

  Final event probability : 
       energy thermalization =    6.3500000000000006E-003
       electronic excitation =    9.5800000000000000E-003
       capture (energetic)   =   0.15766000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8111400000000004     
       vib. excitation, v2   =   0.27941000000000005     
       vib. excitation, v13  =   0.66158000000000006     
       vib. excitation, vr   =   0.16485000000000002     
       Phonon excitation, t1 =    4.9860000000000002E-002
       Phonon excitation, t2 =    1.7556300000000002     
       Phonon excitation, l1 =    1.1255500000000001     
       Phonon excitation, l2 =    3.5703200000000002     
       electronic excitation =    1.0726800000000001     
               a) ionization =   0.72685000000000011     
               b) excitation =   0.34583000000000003     
       electron capture      =   0.98407000000000011     

