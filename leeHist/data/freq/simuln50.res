  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    50.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.510756388379420        34.726974907159608       nm 
  Mean penetration     =    13.175125757448539        9.2310616042602298       nm 
  Mean radial distance =    9.6687793091369425        8.0776936965613846       nm 
  Mean axial distance  =    7.3750794295120388        6.7579739338321794       nm 

  Final event probability : 
       energy thermalization =    4.3700000000000006E-003
       electronic excitation =    8.9000000000000006E-004
       capture (energetic)   =   0.31179000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8802400000000001     
       vib. excitation, v2   =   0.30386000000000002     
       vib. excitation, v13  =   0.70002000000000009     
       vib. excitation, vr   =   0.19663000000000003     
       Phonon excitation, t1 =    7.5500000000000012E-002
       Phonon excitation, t2 =    1.8951600000000002     
       Phonon excitation, l1 =    1.2849500000000000     
       Phonon excitation, l2 =    3.7504100000000005     
       electronic excitation =    2.0439100000000003     
               a) ionization =    1.7585300000000001     
               b) excitation =   0.28538000000000002     
       electron capture      =   0.99474000000000007     

