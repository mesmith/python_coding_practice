  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    23.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    34.647160587344089        33.972207861230714       nm 
  Mean penetration     =    11.970045071396729        9.1166313555356133       nm 
  Mean radial distance =    8.6271708504143341        7.9611538825779506       nm 
  Mean axial distance  =    6.8011843525165503        6.5062116214245949       nm 

  Final event probability : 
       energy thermalization =    3.7000000000000002E-003
       electronic excitation =    6.5600000000000007E-003
       capture (energetic)   =   0.23023000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.6581600000000001     
       vib. excitation, v2   =   0.26081000000000004     
       vib. excitation, v13  =   0.64535000000000009     
       vib. excitation, vr   =   0.19465000000000002     
       Phonon excitation, t1 =    3.6550000000000006E-002
       Phonon excitation, t2 =    1.8323300000000002     
       Phonon excitation, l1 =    1.1605300000000001     
       Phonon excitation, l2 =    3.7377300000000004     
       electronic excitation =    1.1451500000000001     
               a) ionization =   0.94826000000000010     
               b) excitation =   0.19689000000000001     
       electron capture      =   0.98974000000000006     

