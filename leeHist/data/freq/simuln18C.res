  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    18.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    31.710542001635719        29.669221602142816       nm 
  Mean penetration     =    11.384234803676490        8.6503527329790568       nm 
  Mean radial distance =    8.1330337352324324        7.5135657384776859       nm 
  Mean axial distance  =    6.5297753588606167        6.2603139208953182       nm 

  Final event probability : 
       energy thermalization =    6.9600000000000009E-003
       electronic excitation =    6.0000000000000008E-005
       capture (energetic)   =   0.12471000000000002     
       reactive capture      =    3.5700000000000003E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.6339700000000001     
       vib. excitation, v2   =   0.26122000000000001     
       vib. excitation, v13  =   0.61303000000000007     
       vib. excitation, vr   =   0.14988000000000001     
       Phonon excitation, t1 =    5.1890000000000006E-002
       Phonon excitation, t2 =    1.6339400000000002     
       Phonon excitation, l1 =    1.0598000000000001     
       Phonon excitation, l2 =    3.3631600000000001     
       electronic excitation =    1.0558900000000000     
               a) ionization =   0.72191000000000005     
               b) excitation =   0.33398000000000005     
       electron capture      =   0.98938000000000004     

