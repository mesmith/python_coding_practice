  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    6.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    60.219064300650018        43.460874552016612       nm 
  Mean penetration     =    16.810745477755628        11.037999057027585       nm 
  Mean radial distance =    12.627293470923121        9.8129176379855387       nm 
  Mean axial distance  =    9.0187206393416481        8.2072755381815821       nm 

  Final event probability : 
       energy thermalization =    2.0000000000000002E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.45534000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    5.3485500000000004     
       vib. excitation, v2   =   0.47650000000000003     
       vib. excitation, v13  =    1.2203700000000002     
       vib. excitation, vr   =   0.45969000000000004     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    3.2536500000000004     
       Phonon excitation, l1 =    1.7860900000000002     
       Phonon excitation, l2 =    6.6022900000000009     
       electronic excitation =   0.29352000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.29352000000000000     
       electron capture      =   0.99998000000000009     

