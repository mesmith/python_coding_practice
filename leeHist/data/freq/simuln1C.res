  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    1.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    1.4930590320443717        1.3082770825317420       nm 
  Mean penetration     =    1.2702019242489233        1.1365717570226532       nm 
  Mean radial distance =   0.26013074595607666       0.51235001162874516       nm 
  Mean axial distance  =    1.1512329897494544        1.1178999736703068       nm 

  Final event probability : 
       energy thermalization =    9.4000000000000008E-004
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       reactive capture      =    1.7780000000000001E-002
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.12244000000000001     
       vib. excitation, v2   =    7.4900000000000010E-003
       vib. excitation, v13  =    3.3690000000000005E-002
       vib. excitation, vr   =    3.3000000000000005E-004
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =   0.10821000000000001     
       Phonon excitation, l1 =    6.6450000000000009E-002
       Phonon excitation, l2 =   0.28466000000000002     
       electronic excitation =    0.0000000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =    0.0000000000000000     
       electron capture      =   0.98128000000000004     

