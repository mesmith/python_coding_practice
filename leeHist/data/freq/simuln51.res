  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    51.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.520035864239617        34.902191610463383       nm 
  Mean penetration     =    13.187220511495312        9.2248964773778876       nm 
  Mean radial distance =    9.6653106408021241        8.0436019986246023       nm 
  Mean axial distance  =    7.3980962013058962        6.7935196722969122       nm 

  Final event probability : 
       energy thermalization =    4.2800000000000000E-003
       electronic excitation =    7.9000000000000001E-004
       capture (energetic)   =   0.31292000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9049200000000002     
       vib. excitation, v2   =   0.30403000000000002     
       vib. excitation, v13  =   0.69578000000000007     
       vib. excitation, vr   =   0.19858000000000001     
       Phonon excitation, t1 =    7.4660000000000004E-002
       Phonon excitation, t2 =    1.8963500000000002     
       Phonon excitation, l1 =    1.2858100000000001     
       Phonon excitation, l2 =    3.7633600000000005     
       electronic excitation =    2.0731700000000002     
               a) ionization =    1.7844600000000002     
               b) excitation =   0.28871000000000002     
       electron capture      =   0.99493000000000009     

