  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    21.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    32.532845760754519        32.521620025406065       nm 
  Mean penetration     =    11.496901680719674        8.9090920579961583       nm 
  Mean radial distance =    8.1933480787103488        7.7222797815893278       nm 
  Mean axial distance  =    6.6122318400648146        6.4081590088865381       nm 

  Final event probability : 
       energy thermalization =    4.5100000000000001E-003
       electronic excitation =    8.5100000000000002E-003
       capture (energetic)   =   0.19526000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.5090200000000000     
       vib. excitation, v2   =   0.25268000000000002     
       vib. excitation, v13  =   0.61099000000000003     
       vib. excitation, vr   =   0.17109000000000002     
       Phonon excitation, t1 =    3.7140000000000006E-002
       Phonon excitation, t2 =    1.7237400000000000     
       Phonon excitation, l1 =    1.0982300000000000     
       Phonon excitation, l2 =    3.5101500000000003     
       electronic excitation =    1.1077100000000002     
               a) ionization =   0.90649000000000013     
               b) excitation =   0.20122000000000001     
       electron capture      =   0.98698000000000008     

