  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    3.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    32.089978975583698        19.857710845937358       nm 
  Mean penetration     =    11.989601103096245        7.4341714209829322       nm 
  Mean radial distance =    8.5523187745831812        6.5336424353870148       nm 
  Mean axial distance  =    6.8710759110169803        5.9979259129626916       nm 

  Final event probability : 
       energy thermalization =    8.0000000000000007E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.3531500000000003     
       vib. excitation, v2   =   0.19852000000000003     
       vib. excitation, v13  =   0.67992000000000008     
       vib. excitation, vr   =   0.18759000000000001     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    1.9999600000000002     
       Phonon excitation, l1 =    1.1447500000000002     
       Phonon excitation, l2 =    4.3570300000000000     
       electronic excitation =    1.0000000000000000E-003
               a) ionization =    0.0000000000000000     
               b) excitation =    1.0000000000000000E-003
       electron capture      =   0.99992000000000003     

