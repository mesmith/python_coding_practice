  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    55.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.821195948643343        34.572824393972404       nm 
  Mean penetration     =    13.300747318961049        9.2076487502348421       nm 
  Mean radial distance =    9.7727632616039983        8.0884197615381748       nm 
  Mean axial distance  =    7.4381334014705436        6.7405793702233181       nm 

  Final event probability : 
       energy thermalization =    4.0500000000000006E-003
       electronic excitation =    7.1000000000000002E-004
       capture (energetic)   =   0.31206000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9015500000000003     
       vib. excitation, v2   =   0.31197000000000003     
       vib. excitation, v13  =   0.69933000000000001     
       vib. excitation, vr   =   0.19635000000000002     
       Phonon excitation, t1 =    8.1080000000000013E-002
       Phonon excitation, t2 =    1.9074700000000002     
       Phonon excitation, l1 =    1.3039300000000000     
       Phonon excitation, l2 =    3.7495500000000002     
       electronic excitation =    2.1941200000000003     
               a) ionization =    1.8987800000000001     
               b) excitation =   0.29534000000000005     
       electron capture      =   0.99524000000000012     

