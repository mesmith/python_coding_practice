  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    12.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    19.948503074371160        13.229355974380086       nm 
  Mean penetration     =    9.9779134757829127        6.4293294929316760       nm 
  Mean radial distance =    6.5118943155524116        5.2902449544670409       nm 
  Mean axial distance  =    6.3263801441326777        5.5209138804755797       nm 

  Final event probability : 
       energy thermalization =    3.2700000000000003E-003
       electronic excitation =    1.0000000000000001E-005
       capture (energetic)   =   0.61245000000000005     
       reactive capture      =    5.0800000000000003E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    1.5352100000000002     
       vib. excitation, v2   =   0.15569000000000002     
       vib. excitation, v13  =   0.44446000000000002     
       vib. excitation, vr   =   0.10053000000000001     
       Phonon excitation, t1 =    1.0660000000000001E-002
       Phonon excitation, t2 =   0.71140000000000003     
       Phonon excitation, l1 =   0.63407000000000002     
       Phonon excitation, l2 =    1.6808400000000001     
       electronic excitation =   0.38354000000000005     
               a) ionization =   0.10772000000000001     
               b) excitation =   0.27582000000000001     
       electron capture      =   0.99164000000000008     

