  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    75.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.002321199060134        34.940865657596788       nm 
  Mean penetration     =    13.588190245880849        9.2056857205598153       nm 
  Mean radial distance =    10.085448915572293        8.0971004552067587       nm 
  Mean axial distance  =    7.4932584085479652        6.7790358131792638       nm 

  Final event probability : 
       energy thermalization =    4.8100000000000000E-003
       electronic excitation =    5.7000000000000009E-004
       capture (energetic)   =   0.30678000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9118800000000000     
       vib. excitation, v2   =   0.32067000000000001     
       vib. excitation, v13  =   0.70674000000000003     
       vib. excitation, vr   =   0.19732000000000002     
       Phonon excitation, t1 =    7.8400000000000011E-002
       Phonon excitation, t2 =    1.9853600000000002     
       Phonon excitation, l1 =    1.3772700000000002     
       Phonon excitation, l2 =    3.8360900000000004     
       electronic excitation =    2.7317400000000003     
               a) ionization =    2.4278700000000004     
               b) excitation =   0.30387000000000003     
       electron capture      =   0.99462000000000006     

