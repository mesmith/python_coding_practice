  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    22.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    33.412321282872796        33.286102577330198       nm 
  Mean penetration     =    11.665644031805970        9.0143100867688002       nm 
  Mean radial distance =    8.3853948665642761        7.8682192085319134       nm 
  Mean axial distance  =    6.6404125722440197        6.4051726991474514       nm 

  Final event probability : 
       energy thermalization =    3.8900000000000002E-003
       electronic excitation =    7.6100000000000004E-003
       capture (energetic)   =   0.20948000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.5653400000000004     
       vib. excitation, v2   =   0.25489000000000001     
       vib. excitation, v13  =   0.62652000000000008     
       vib. excitation, vr   =   0.18189000000000002     
       Phonon excitation, t1 =    3.6170000000000001E-002
       Phonon excitation, t2 =    1.7693600000000000     
       Phonon excitation, l1 =    1.1247100000000001     
       Phonon excitation, l2 =    3.6055200000000003     
       electronic excitation =    1.1273500000000001     
               a) ionization =   0.92975000000000008     
               b) excitation =   0.19760000000000003     
       electron capture      =   0.98850000000000005     

