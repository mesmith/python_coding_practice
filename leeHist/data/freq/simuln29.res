  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    29.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.875458051507813        35.171968868327156       nm 
  Mean penetration     =    12.296043474331899        9.2712532874577231       nm 
  Mean radial distance =    8.9514436009397222        8.0876475426400489       nm 
  Mean axial distance  =    6.9218302327798815        6.6104994937335047       nm 

  Final event probability : 
       energy thermalization =    3.4500000000000004E-003
       electronic excitation =    3.1300000000000004E-003
       capture (energetic)   =   0.38462000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7941100000000003     
       vib. excitation, v2   =   0.28476000000000001     
       vib. excitation, v13  =   0.69274000000000002     
       vib. excitation, vr   =   0.20928000000000002     
       Phonon excitation, t1 =    4.4340000000000004E-002
       Phonon excitation, t2 =    1.8189300000000002     
       Phonon excitation, l1 =    1.1941200000000001     
       Phonon excitation, l2 =    3.6736200000000001     
       electronic excitation =    1.3028100000000000     
               a) ionization =    1.0548700000000002     
               b) excitation =   0.24794000000000002     
       electron capture      =   0.99342000000000008     

