  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    82.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.387857109355210        34.781022896089063       nm 
  Mean penetration     =    13.733403285814008        9.2116316093259218       nm 
  Mean radial distance =    10.190527879876653        8.0881221213310823       nm 
  Mean axial distance  =    7.5824209604777497        6.8339474079585019       nm 

  Final event probability : 
       energy thermalization =    4.7600000000000003E-003
       electronic excitation =    4.9000000000000009E-004
       capture (energetic)   =   0.30449000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9094700000000002     
       vib. excitation, v2   =   0.32603000000000004     
       vib. excitation, v13  =   0.71050000000000002     
       vib. excitation, vr   =   0.19440000000000002     
       Phonon excitation, t1 =    7.7900000000000011E-002
       Phonon excitation, t2 =    1.9868900000000003     
       Phonon excitation, l1 =    1.4018800000000000     
       Phonon excitation, l2 =    3.8379300000000005     
       electronic excitation =    2.9130600000000002     
               a) ionization =    2.6005000000000003     
               b) excitation =   0.31256000000000000     
       electron capture      =   0.99475000000000013     

