  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    96.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.417473996206738        34.707537553499115       nm 
  Mean penetration     =    14.058405890259229        9.2051730453383058       nm 
  Mean radial distance =    10.393489446182009        8.1083843363061163       nm 
  Mean axial distance  =    7.8002250061777820        6.9108579358676936       nm 

  Final event probability : 
       energy thermalization =    4.5400000000000006E-003
       electronic excitation =    4.5000000000000004E-004
       capture (energetic)   =   0.30738000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9038700000000004     
       vib. excitation, v2   =   0.33299000000000001     
       vib. excitation, v13  =   0.71413000000000004     
       vib. excitation, vr   =   0.19583000000000000     
       Phonon excitation, t1 =    7.8370000000000009E-002
       Phonon excitation, t2 =    2.0250400000000002     
       Phonon excitation, l1 =    1.4337800000000001     
       Phonon excitation, l2 =    3.8894100000000003     
       electronic excitation =    3.2402200000000003     
               a) ionization =    2.9266400000000004     
               b) excitation =   0.31358000000000003     
       electron capture      =   0.99501000000000006     

