  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    79.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.842127884554110        34.643503250350214       nm 
  Mean penetration     =    13.576563819691083        9.1755817846169521       nm 
  Mean radial distance =    10.095296473571267        8.0633843336936106       nm 
  Mean axial distance  =    7.4733573688090980        6.7624062338972033       nm 

  Final event probability : 
       energy thermalization =    4.8700000000000002E-003
       electronic excitation =    6.1000000000000008E-004
       capture (energetic)   =   0.30871000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8818000000000001     
       vib. excitation, v2   =   0.32185000000000002     
       vib. excitation, v13  =   0.70664000000000005     
       vib. excitation, vr   =   0.19216000000000003     
       Phonon excitation, t1 =    7.6880000000000004E-002
       Phonon excitation, t2 =    1.9719900000000001     
       Phonon excitation, l1 =    1.3831000000000002     
       Phonon excitation, l2 =    3.8112700000000004     
       electronic excitation =    2.8307400000000000     
               a) ionization =    2.5239000000000003     
               b) excitation =   0.30684000000000000     
       electron capture      =   0.99452000000000007     

