  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    6.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    50.245591868010429        32.922676596285484       nm 
  Mean penetration     =    15.707177591573219        9.7999301296861692       nm 
  Mean radial distance =    11.752690895259976        8.7231410746981783       nm 
  Mean axial distance  =    8.4943305848255601        7.5087597422639814       nm 

  Final event probability : 
       energy thermalization =    0.0000000000000000     
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.30174000000000001     
       reactive capture      =    3.6200000000000004E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    4.2690400000000004     
       vib. excitation, v2   =   0.36687000000000003     
       vib. excitation, v13  =   0.95188000000000006     
       vib. excitation, vr   =   0.36329000000000006     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.7391700000000001     
       Phonon excitation, l1 =    1.5085600000000001     
       Phonon excitation, l2 =    5.6099100000000002     
       electronic excitation =   0.25248000000000004     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.25248000000000004     
       electron capture      =   0.99638000000000004     

