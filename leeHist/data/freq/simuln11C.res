  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    11.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    7.6426312740806779        9.1623700779428834       nm 
  Mean penetration     =    4.9122352244446787        4.1951161846879961       nm 
  Mean radial distance =    2.2453942811189589        3.1679812914788963       nm 
  Mean axial distance  =    3.7411518361102893        3.5573777844969179       nm 

  Final event probability : 
       energy thermalization =    1.3600000000000001E-003
       electronic excitation =    1.0000000000000001E-005
       capture (energetic)   =   0.85486000000000006     
       reactive capture      =    4.3700000000000006E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.53251000000000004     
       vib. excitation, v2   =    6.0700000000000004E-002
       vib. excitation, v13  =   0.19181000000000001     
       vib. excitation, vr   =    5.1350000000000007E-002
       Phonon excitation, t1 =    1.5000000000000000E-003
       Phonon excitation, t2 =   0.25240000000000001     
       Phonon excitation, l1 =   0.24525000000000002     
       Phonon excitation, l2 =   0.63248000000000004     
       electronic excitation =   0.14004000000000000     
               a) ionization =    3.0340000000000002E-002
               b) excitation =   0.10970000000000001     
       electron capture      =   0.99424000000000012     

