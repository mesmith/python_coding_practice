  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    5.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    42.471514416964446        29.068565561536438       nm 
  Mean penetration     =    14.505263318999488        9.2568215867647936       nm 
  Mean radial distance =    10.609355530175900        8.1989060458799035       nm 
  Mean axial distance  =    8.0742613937234644        7.1496312963461968       nm 

  Final event probability : 
       energy thermalization =    7.0000000000000007E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.18780000000000002     
       reactive capture      =    3.1400000000000004E-003
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.3456400000000004     
       vib. excitation, v2   =   0.27595000000000003     
       vib. excitation, v13  =   0.74201000000000006     
       vib. excitation, vr   =   0.28545000000000004     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.3030900000000001     
       Phonon excitation, l1 =    1.2895900000000000     
       Phonon excitation, l2 =    4.8072100000000004     
       electronic excitation =   0.20146000000000003     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.20146000000000003     
       electron capture      =   0.99679000000000006     

