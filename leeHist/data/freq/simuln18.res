  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    18.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    32.013847250582955        32.284144064756397       nm 
  Mean penetration     =    11.316779446832944        8.7516027316040059       nm 
  Mean radial distance =    8.0455412830049209        7.6234236644067499       nm 
  Mean axial distance  =    6.5253929218738245        6.2635430653638720       nm 

  Final event probability : 
       energy thermalization =    5.5600000000000007E-003
       electronic excitation =    9.9200000000000000E-003
       capture (energetic)   =   0.16920000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.6076400000000004     
       vib. excitation, v2   =   0.26153000000000004     
       vib. excitation, v13  =   0.62702000000000002     
       vib. excitation, vr   =   0.16389000000000001     
       Phonon excitation, t1 =    4.4320000000000005E-002
       Phonon excitation, t2 =    1.6971400000000001     
       Phonon excitation, l1 =    1.0779300000000001     
       Phonon excitation, l2 =    3.4485400000000004     
       electronic excitation =    1.0754100000000000     
               a) ionization =   0.79338000000000009     
               b) excitation =   0.28203000000000000     
       electron capture      =   0.98452000000000006     

