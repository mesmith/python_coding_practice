  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    68.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.564218941511484        34.690829196952507       nm 
  Mean penetration     =    13.523844682411301        9.1924969753270105       nm 
  Mean radial distance =    9.9273045433790390        8.0385946472343388       nm 
  Mean axial distance  =    7.5760217818567304        6.8432368216126020       nm 

  Final event probability : 
       energy thermalization =    4.5800000000000007E-003
       electronic excitation =    5.4000000000000001E-004
       capture (energetic)   =   0.30697000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8770600000000002     
       vib. excitation, v2   =   0.31453000000000003     
       vib. excitation, v13  =   0.69685000000000008     
       vib. excitation, vr   =   0.19267000000000001     
       Phonon excitation, t1 =    8.4870000000000001E-002
       Phonon excitation, t2 =    1.9429200000000002     
       Phonon excitation, l1 =    1.3527700000000000     
       Phonon excitation, l2 =    3.7864200000000001     
       electronic excitation =    2.5517100000000004     
               a) ionization =    2.2508800000000000     
               b) excitation =   0.30083000000000004     
       electron capture      =   0.99488000000000010     

