  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    36.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.025937507762542        34.537308387751892       nm 
  Mean penetration     =    12.669445080197564        9.2235932127569065       nm 
  Mean radial distance =    9.3489327011993879        8.0756948717455845       nm 
  Mean axial distance  =    7.0255450212156969        6.6039258957331013       nm 

  Final event probability : 
       energy thermalization =    4.8300000000000001E-003
       electronic excitation =    1.7600000000000001E-003
       capture (energetic)   =   0.31103000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9101600000000003     
       vib. excitation, v2   =   0.29749000000000003     
       vib. excitation, v13  =   0.69203000000000003     
       vib. excitation, vr   =   0.19205000000000003     
       Phonon excitation, t1 =    6.7130000000000009E-002
       Phonon excitation, t2 =    1.8223400000000001     
       Phonon excitation, l1 =    1.2499600000000002     
       Phonon excitation, l2 =    3.6889300000000005     
       electronic excitation =    1.6233500000000001     
               a) ionization =    1.3063600000000002     
               b) excitation =   0.31699000000000005     
       electron capture      =   0.99341000000000013     

