  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    80.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.235389718424400        34.718561931158909       nm 
  Mean penetration     =    13.664687286831823        9.2105821416861300       nm 
  Mean radial distance =    10.158995447109991        8.0921516100045352       nm 
  Mean axial distance  =    7.5226214622550565        6.8029818607408599       nm 

  Final event probability : 
       energy thermalization =    4.7000000000000002E-003
       electronic excitation =    3.9000000000000005E-004
       capture (energetic)   =   0.30663000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9074500000000003     
       vib. excitation, v2   =   0.32377000000000000     
       vib. excitation, v13  =   0.71227000000000007     
       vib. excitation, vr   =   0.19627000000000003     
       Phonon excitation, t1 =    7.7310000000000004E-002
       Phonon excitation, t2 =    1.9821600000000001     
       Phonon excitation, l1 =    1.3940100000000002     
       Phonon excitation, l2 =    3.8527000000000005     
       electronic excitation =    2.8575600000000003     
               a) ionization =    2.5487700000000002     
               b) excitation =   0.30879000000000001     
       electron capture      =   0.99491000000000007     

