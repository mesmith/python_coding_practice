  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    8.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    29.427896038835534        37.885825666400805       nm 
  Mean penetration     =    9.7330161631820342        8.6668403784532142       nm 
  Mean radial distance =    6.7500032029039119        7.5044670525226715       nm 
  Mean axial distance  =    5.7008091537440411        5.9554119747776744       nm 

  Final event probability : 
       energy thermalization =    6.9700000000000005E-003
       electronic excitation =    1.3070000000000002E-002
       capture (energetic)   =   0.79642000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.6668400000000001     
       vib. excitation, v2   =   0.31252000000000002     
       vib. excitation, v13  =   0.92391000000000012     
       vib. excitation, vr   =   0.31540000000000001     
       Phonon excitation, t1 =    8.3800000000000003E-003
       Phonon excitation, t2 =    1.5588200000000001     
       Phonon excitation, l1 =   0.99020000000000008     
       Phonon excitation, l2 =    3.2903600000000002     
       electronic excitation =   0.16467000000000001     
               a) ionization =    1.9100000000000002E-003
               b) excitation =   0.16276000000000002     
       electron capture      =   0.97996000000000005     

