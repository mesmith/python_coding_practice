  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    7.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    43.043244919665639        46.311810721964015       nm 
  Mean penetration     =    12.449240875298347        10.649057521366590       nm 
  Mean radial distance =    9.0660467256387847        9.2790078114246697       nm 
  Mean axial distance  =    6.9003850608188548        7.2441370280426067       nm 

  Final event probability : 
       energy thermalization =    2.0000000000000002E-005
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =   0.75809000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    4.0278499999999999     
       vib. excitation, v2   =   0.40160000000000001     
       vib. excitation, v13  =    1.0951700000000002     
       vib. excitation, vr   =   0.37929000000000002     
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =    2.3818900000000003     
       Phonon excitation, l1 =    1.3435700000000002     
       Phonon excitation, l2 =    4.8216000000000001     
       electronic excitation =   0.13262000000000002     
               a) ionization =    0.0000000000000000     
               b) excitation =   0.13262000000000002     
       electron capture      =   0.99998000000000009     

