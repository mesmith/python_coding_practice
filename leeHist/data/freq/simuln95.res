  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    95.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.171909946484391        34.738416447183205       nm 
  Mean penetration     =    13.966142560025114        9.1913380072048607       nm 
  Mean radial distance =    10.312520782563471        8.0819982294636308       nm 
  Mean axial distance  =    7.7571820948496910        6.9060247642995174       nm 

  Final event probability : 
       energy thermalization =    4.3000000000000000E-003
       electronic excitation =    4.0000000000000002E-004
       capture (energetic)   =   0.30828000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8866700000000001     
       vib. excitation, v2   =   0.33293000000000000     
       vib. excitation, v13  =   0.71107000000000009     
       vib. excitation, vr   =   0.19462000000000002     
       Phonon excitation, t1 =    7.8380000000000005E-002
       Phonon excitation, t2 =    2.0060600000000002     
       Phonon excitation, l1 =    1.4239500000000000     
       Phonon excitation, l2 =    3.8472700000000004     
       electronic excitation =    3.2154600000000002     
               a) ionization =    2.9019500000000003     
               b) excitation =   0.31351000000000001     
       electron capture      =   0.99530000000000007     

