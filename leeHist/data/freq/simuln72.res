  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    72.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.849272138506329        34.754111684073948       nm 
  Mean penetration     =    13.538368837038965        9.2053183157774043       nm 
  Mean radial distance =    10.033745443548922        8.0739380924754549       nm 
  Mean axial distance  =    7.4770634315715272        6.8010524594127082       nm 

  Final event probability : 
       energy thermalization =    4.1600000000000005E-003
       electronic excitation =    5.2000000000000006E-004
       capture (energetic)   =   0.30843000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9004000000000003     
       vib. excitation, v2   =   0.32097000000000003     
       vib. excitation, v13  =   0.70789000000000002     
       vib. excitation, vr   =   0.19778000000000001     
       Phonon excitation, t1 =    8.0870000000000011E-002
       Phonon excitation, t2 =    1.9712000000000001     
       Phonon excitation, l1 =    1.3760900000000000     
       Phonon excitation, l2 =    3.8169600000000004     
       electronic excitation =    2.6550300000000000     
               a) ionization =    2.3492800000000003     
               b) excitation =   0.30575000000000002     
       electron capture      =   0.99532000000000009     

