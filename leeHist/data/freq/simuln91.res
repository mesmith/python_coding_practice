  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    91.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.263906176774739        34.887416781452551       nm 
  Mean penetration     =    13.995287537969620        9.2179713069748690       nm 
  Mean radial distance =    10.346278035730014        8.1073692505118959       nm 
  Mean axial distance  =    7.7729442232935471        6.9025720591275999       nm 

  Final event probability : 
       energy thermalization =    4.5100000000000001E-003
       electronic excitation =    3.1000000000000000E-004
       capture (energetic)   =   0.30607000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9194000000000004     
       vib. excitation, v2   =   0.33197000000000004     
       vib. excitation, v13  =   0.71356000000000008     
       vib. excitation, vr   =   0.19882000000000002     
       Phonon excitation, t1 =    7.9080000000000011E-002
       Phonon excitation, t2 =    2.0185400000000002     
       Phonon excitation, l1 =    1.4246400000000001     
       Phonon excitation, l2 =    3.8734800000000003     
       electronic excitation =    3.1267200000000002     
               a) ionization =    2.8169400000000002     
               b) excitation =   0.30978000000000000     
       electron capture      =   0.99518000000000006     

