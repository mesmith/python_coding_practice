  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    83.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.289120377623568        34.843100422885200       nm 
  Mean penetration     =    13.689652886519804        9.2452301596980941       nm 
  Mean radial distance =    10.168812188672090        8.1327597234449502       nm 
  Mean axial distance  =    7.5434543185734899        6.8140040912091440       nm 

  Final event probability : 
       energy thermalization =    4.5500000000000002E-003
       electronic excitation =    4.8000000000000007E-004
       capture (energetic)   =   0.30694000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8976400000000004     
       vib. excitation, v2   =   0.32627000000000000     
       vib. excitation, v13  =   0.70256000000000007     
       vib. excitation, vr   =   0.19782000000000002     
       Phonon excitation, t1 =    7.8120000000000009E-002
       Phonon excitation, t2 =    1.9841700000000002     
       Phonon excitation, l1 =    1.3930800000000001     
       Phonon excitation, l2 =    3.8304300000000002     
       electronic excitation =    2.9315700000000002     
               a) ionization =    2.6212300000000002     
               b) excitation =   0.31034000000000000     
       electron capture      =   0.99497000000000013     

