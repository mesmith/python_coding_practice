  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    69.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    39.873086537218796        34.866108917421570       nm 
  Mean penetration     =    13.584878726895935        9.2467217901669922       nm 
  Mean radial distance =    10.011005024504239        8.0871847161695900       nm 
  Mean axial distance  =    7.5713521580940295        6.8631364661681751       nm 

  Final event probability : 
       energy thermalization =    4.1500000000000000E-003
       electronic excitation =    4.7000000000000004E-004
       capture (energetic)   =   0.30730000000000002     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9120200000000001     
       vib. excitation, v2   =   0.31765000000000004     
       vib. excitation, v13  =   0.70305000000000006     
       vib. excitation, vr   =   0.19387000000000001     
       Phonon excitation, t1 =    8.3860000000000004E-002
       Phonon excitation, t2 =    1.9578600000000002     
       Phonon excitation, l1 =    1.3676400000000002     
       Phonon excitation, l2 =    3.8155000000000001     
       electronic excitation =    2.5807600000000002     
               a) ionization =    2.2767000000000004     
               b) excitation =   0.30406000000000005     
       electron capture      =   0.99538000000000004     

