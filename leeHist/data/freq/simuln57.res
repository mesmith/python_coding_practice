  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    57.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    38.765879874035242        34.489509728244620       nm 
  Mean penetration     =    13.265121572759810        9.1478548781711879       nm 
  Mean radial distance =    9.7529248948078688        8.0581996104692433       nm 
  Mean axial distance  =    7.4009356844871332        6.6946788750720536       nm 

  Final event probability : 
       energy thermalization =    4.2900000000000004E-003
       electronic excitation =    8.2000000000000009E-004
       capture (energetic)   =   0.31186000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8803000000000001     
       vib. excitation, v2   =   0.30978000000000000     
       vib. excitation, v13  =   0.69682000000000011     
       vib. excitation, vr   =   0.19293000000000002     
       Phonon excitation, t1 =    8.2160000000000011E-002
       Phonon excitation, t2 =    1.9005400000000001     
       Phonon excitation, l1 =    1.3048600000000001     
       Phonon excitation, l2 =    3.7379900000000004     
       electronic excitation =    2.2503000000000002     
               a) ionization =    1.9513600000000002     
               b) excitation =   0.29894000000000004     
       electron capture      =   0.99489000000000005     

