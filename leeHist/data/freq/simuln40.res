  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    40.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    37.143772465075052        34.404259177245230       nm 
  Mean penetration     =    12.715351370605831        9.1638560868748389       nm 
  Mean radial distance =    9.3592756216841053        7.9992904101026703       nm 
  Mean axial distance  =    7.0821698385816454        6.6268093384822118       nm 

  Final event probability : 
       energy thermalization =    5.0500000000000007E-003
       electronic excitation =    1.4600000000000001E-003
       capture (energetic)   =   0.28172000000000003     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8737400000000002     
       vib. excitation, v2   =   0.29766000000000004     
       vib. excitation, v13  =   0.67706000000000011     
       vib. excitation, vr   =   0.18963000000000002     
       Phonon excitation, t1 =    7.3220000000000007E-002
       Phonon excitation, t2 =    1.8585800000000001     
       Phonon excitation, l1 =    1.2492500000000002     
       Phonon excitation, l2 =    3.7073900000000002     
       electronic excitation =    1.7660900000000002     
               a) ionization =    1.4643300000000001     
               b) excitation =   0.30176000000000003     
       electron capture      =   0.99349000000000010     

