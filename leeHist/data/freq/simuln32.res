  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    32.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.959263378495287        34.623869926851505       nm 
  Mean penetration     =    12.414292393784004        9.2909173140260570       nm 
  Mean radial distance =    9.0917998983756014        8.0915535402065473       nm 
  Mean axial distance  =    6.9459542866096236        6.6374283526551112       nm 

  Final event probability : 
       energy thermalization =    4.0700000000000007E-003
       electronic excitation =    2.4800000000000000E-003
       capture (energetic)   =   0.36844000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.8008500000000001     
       vib. excitation, v2   =   0.28888000000000003     
       vib. excitation, v13  =   0.68355000000000010     
       vib. excitation, vr   =   0.20222000000000001     
       Phonon excitation, t1 =    5.4380000000000005E-002
       Phonon excitation, t2 =    1.7803500000000001     
       Phonon excitation, l1 =    1.2054400000000001     
       Phonon excitation, l2 =    3.5883900000000004     
       electronic excitation =    1.4372900000000002     
               a) ionization =    1.1483900000000000     
               b) excitation =   0.28890000000000005     
       electron capture      =   0.99345000000000006     

