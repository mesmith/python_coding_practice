  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    89.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.089849872561196        34.953417057741440       nm 
  Mean penetration     =    13.933788691051522        9.2231188902423913       nm 
  Mean radial distance =    10.296533638364830        8.1179361010483184       nm 
  Mean axial distance  =    7.7319322310969856        6.8930488042274636       nm 

  Final event probability : 
       energy thermalization =    4.2600000000000008E-003
       electronic excitation =    3.3000000000000005E-004
       capture (energetic)   =   0.30524000000000001     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9176800000000003     
       vib. excitation, v2   =   0.32995000000000002     
       vib. excitation, v13  =   0.71844000000000008     
       vib. excitation, vr   =   0.19703000000000001     
       Phonon excitation, t1 =    7.8090000000000007E-002
       Phonon excitation, t2 =    2.0134500000000002     
       Phonon excitation, l1 =    1.4206900000000000     
       Phonon excitation, l2 =    3.8739500000000002     
       electronic excitation =    3.0731400000000004     
               a) ionization =    2.7618900000000002     
               b) excitation =   0.31125000000000003     
       electron capture      =   0.99541000000000013     

