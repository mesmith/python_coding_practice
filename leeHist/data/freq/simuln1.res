  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    1.0000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    1.9947935522510662        1.7592570589912653       nm 
  Mean penetration     =    1.4664851800978986        1.2663636752568654       nm 
  Mean radial distance =   0.48860816054133988       0.79798197405727012       nm 
  Mean axial distance  =    1.2223477540334611        1.1766939042071589       nm 

  Final event probability : 
       energy thermalization =    6.1000000000000008E-004
       electronic excitation =    0.0000000000000000     
       capture (energetic)   =    0.0000000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =   0.20820000000000002     
       vib. excitation, v2   =    1.3180000000000001E-002
       vib. excitation, v13  =    5.7260000000000005E-002
       vib. excitation, vr   =    1.1900000000000001E-003
       Phonon excitation, t1 =    0.0000000000000000     
       Phonon excitation, t2 =   0.18307000000000001     
       Phonon excitation, l1 =   0.11182000000000000     
       Phonon excitation, l2 =   0.47830000000000006     
       electronic excitation =    0.0000000000000000     
               a) ionization =    0.0000000000000000     
               b) excitation =    0.0000000000000000     
       electron capture      =   0.99939000000000011     

