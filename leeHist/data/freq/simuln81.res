  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    81.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    40.419195344331889        34.891576848766540       nm 
  Mean penetration     =    13.702157231723813        9.2243213994933626       nm 
  Mean radial distance =    10.172117064787178        8.0831368615239878       nm 
  Mean axial distance  =    7.5554654785227502        6.8515029289213656       nm 

  Final event probability : 
       energy thermalization =    4.6400000000000000E-003
       electronic excitation =    3.8000000000000002E-004
       capture (energetic)   =   0.30794000000000005     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9121000000000001     
       vib. excitation, v2   =   0.32951000000000003     
       vib. excitation, v13  =   0.70918000000000003     
       vib. excitation, vr   =   0.19536000000000001     
       Phonon excitation, t1 =    7.8570000000000001E-002
       Phonon excitation, t2 =    1.9890200000000002     
       Phonon excitation, l1 =    1.3992600000000002     
       Phonon excitation, l2 =    3.8514700000000004     
       electronic excitation =    2.8852100000000003     
               a) ionization =    2.5758700000000001     
               b) excitation =   0.30934000000000000     
       electron capture      =   0.99498000000000009     

