  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    13.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    44.315200517359145        33.017940440354856       nm 
  Mean penetration     =    14.883323601149117        9.9159511149219082       nm 
  Mean radial distance =    10.794174267779157        8.7250975088468987       nm 
  Mean axial distance  =    8.3828680724822355        7.5448927194049533       nm 

  Final event probability : 
       energy thermalization =    5.6400000000000000E-003
       electronic excitation =    5.7800000000000004E-003
       capture (energetic)   =    8.8130000000000014E-002
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    3.7428600000000003     
       vib. excitation, v2   =   0.36442000000000002     
       vib. excitation, v13  =   0.86716000000000004     
       vib. excitation, vr   =   0.19608000000000000     
       Phonon excitation, t1 =    8.5990000000000011E-002
       Phonon excitation, t2 =    1.8823400000000001     
       Phonon excitation, l1 =    1.4027900000000002     
       Phonon excitation, l2 =    4.0808000000000000     
       electronic excitation =   0.96383000000000008     
               a) ionization =   0.35392000000000001     
               b) excitation =   0.60991000000000006     
       electron capture      =   0.98858000000000013     

