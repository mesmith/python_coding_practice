  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    92.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    41.178179561113133        34.803782730613136       nm 
  Mean penetration     =    13.950337370008821        9.2303836385856890       nm 
  Mean radial distance =    10.295001809658000        8.0944406645763323       nm 
  Mean axial distance  =    7.7633106655136350        6.9307914721228805       nm 

  Final event probability : 
       energy thermalization =    4.5400000000000006E-003
       electronic excitation =    4.1000000000000005E-004
       capture (energetic)   =   0.30373000000000000     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.9038100000000004     
       vib. excitation, v2   =   0.33058000000000004     
       vib. excitation, v13  =   0.71489000000000003     
       vib. excitation, vr   =   0.19486000000000001     
       Phonon excitation, t1 =    7.7990000000000004E-002
       Phonon excitation, t2 =    2.0159200000000004     
       Phonon excitation, l1 =    1.4239700000000002     
       Phonon excitation, l2 =    3.8600800000000004     
       electronic excitation =    3.1511300000000002     
               a) ionization =    2.8358500000000002     
               b) excitation =   0.31528000000000000     
       electron capture      =   0.99505000000000010     

