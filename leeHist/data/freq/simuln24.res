  Program Simuln 
  ============== 

  Input 
  ----- 

  Initial cation position 
   0.00    0.00    0.00

  Initial electron position 
   0.00    0.00    0.10

  Initial energy      =    24.000000000000000     
  Final energy        =   0.10000000000000001     

  Simuln method       :            1

  Medium              :            3

  Cross-sections      :            1

  No. of realizations =       100000

  Results 
  -------	

  Mean path length     =    35.608939355319478        34.500939914043208       nm 
  Mean penetration     =    12.211410033387576        9.3021281726747560       nm 
  Mean radial distance =    8.8483830227184725        8.1516248617339713       nm 
  Mean axial distance  =    6.8870444937096540        6.5934718989983772       nm 

  Final event probability : 
       energy thermalization =    3.4300000000000003E-003
       electronic excitation =    5.7500000000000008E-003
       capture (energetic)   =   0.26159000000000004     
       passed through solid  =    0.0000000000000000     
       back scatt from solid =    0.0000000000000000     

  Event frequencies : 
       elastic collision     =    2.7257900000000004     
       vib. excitation, v2   =   0.26996000000000003     
       vib. excitation, v13  =   0.66284000000000010     
       vib. excitation, vr   =   0.20560000000000000     
       Phonon excitation, t1 =    3.6420000000000001E-002
       Phonon excitation, t2 =    1.8780500000000002     
       Phonon excitation, l1 =    1.1831700000000001     
       Phonon excitation, l2 =    3.8119900000000002     
       electronic excitation =    1.1673800000000001     
               a) ionization =   0.96257000000000004     
               b) excitation =   0.20481000000000002     
       electron capture      =   0.99082000000000003     

