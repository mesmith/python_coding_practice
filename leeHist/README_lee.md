this is a mini test repository that is aimed at simply trying to use 
python3 and jupyter notebook to import some histograms from the Low 
Energy Electron (LEE) code. I will hopeully then be able to quickly 
make some graphs and hopefully a surface using the data. 

---
## Files 

1. **Jupyter_main.ipynb:** The main release version of the analysis of
    the mass shooting database (GV_database.csv)
2. **Jupyter_working.ipynb:** a working file that currently has an 
    example of a histogram plot with a gaussian, gamma and exponential 
    best fit lines 

### Files no longer used:

---

## What you need to run 

- This is now run through Jupyter notebook which brings you to your 
    default web browser 
    - open a terminal and navigate to this folder 
    - enter 'Jupyter notebook' and hit enter 
    - this will open your default browser with all the files in the  
        local directory listed
    - click on the jupyter_main.ipynb to open the latest working version 
        of the analysis 

- **numpy** 
    - easily installed with pip3 
- **shapley** 
    - if using conda this should be downloaded when cartopy is
        - geometry
- **matplotlib** 
    - installed with pip3 (was already installed)
        - pyplot 
        - patches  
- **Jupyter notebook** 
    - installed using conda

---

## Git Repository

This is a repo that is located on BitBucket, which runs git. 
Important instructions: 

- git status: shows the status of the files changed in the local dir. 
- git pull: pulls from the repo the lastest chages if working with a 
    group/ multiple computers- this is important to avoid merge errors
- git add <filename> or git add . 
    - <filename> adds that specific file to be committed 
    - . adds all the changed files 
- git commit -m 'message for commit'
    - without the -m then git will use the default editor to force a 
    commit message, which is usually vi/vim (eww)
- git push 
    - pushed all the changes from the local computer to the repo 
